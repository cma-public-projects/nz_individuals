
addStudentWorker_attributes <- function(individual_nodelist,workerEducation_SA2) {
  
  #load in data on the worker/student distribution
  #we will use this to inform which individuals get assigned as students and workers, or just students or just workers.
  
  #workerEducation_SA2 <- read_csv(here("inputs","tripleCounts_workEducation_sa2_backfilled.csv")) %>%
     #  rename(#rename worker and student cols
      #        worker = "work_ind",
       #       student = "education_ind"
        #      ) 
  # workerEducation_SA2 <- read_csv(here("inputs","workEducation_tripleCounts_sa2_safe.csv")) %>%
  #   rename(SA22018 = "ur_sa2",
  #          prop_SA2 = "prop") %>%
  #   select(-triple_count_total) %>%
  #   left_join(concordance %>% select(SA22018,TA2018), by = "SA22018")
  # 
  # workerEducation_TA <- read_csv(here("inputs","workEducation_tripleCounts_ta_safe.csv")) %>%
  #   rename(TA2018 = "ur_ta",
  #          prop_TA = "prop")%>%
  #   select(-triple_count_total)
  # 
  # workerEducation_SA2 <- workerEducation_SA2 %>%
  #   left_join(workerEducation_TA, by = c("TA2018","sex","ethnicity","age_band","work_ind","education_ind")) %>%
  #   select("SA22018", "sex","ethnicity","age_band","work_ind","education_ind", "prop_SA2", "prop_TA") %>%
  #   
  #   #rename worker and student cols
  #   rename(worker = "work_ind",
  #          student = "education_ind") %>%
  #   
  #   #now clean up prop column so if SA2 is missing use TA (if that's missing use NZ)
  #   #for each triple category
  #   group_by(SA22018, sex,ethnicity,age_band) %>%
  #   mutate(probability = case_when(
  #     #if sa2 props are suppressed, use TA info
  #     prop_SA2 == -999999 & prop_TA != -999999 ~ prop_TA,
  #     #if TA info is suppressed, just make categories equally as likely
  #     prop_SA2 == -999999 & prop_TA == -999999 ~ 1/n(),
  #     TRUE ~ prop_SA2)
  #   ) %>%
  #   ungroup %>%
  #   select(SA22018,sex,ethnicity,age_band,student,worker,probability) %>%
  #   
  #   #fill any missing student/worker probability with 0
  #   complete(SA22018,sex,ethnicity,age_band,student,worker,fill = list(probability=0))  %>%
  #   
  #   arrange(SA22018,sex,ethnicity,age_band,student,worker)
  
  #take only those who are workers and standardised probabilities of them being students (Y/N)
  probability_of_student_being_worker_df <- workerEducation_SA2 %>%
    group_by(SA22018,sex,ethnicity,age_band) %>%
    filter(student == "Y") %>%
    #now remove the probability of student == N, we don't need it
    filter(worker == "Y") %>%
    rename(probability_of_student_being_worker = "probability") %>%
    select(SA22018,sex,ethnicity,age_band,student,count_workersEducation,probability_of_student_being_worker)
  
  
#We need a function that will return a vector of Y's and N's according to the probability of a student being a worker.
#we'll put this in a function as we need to ensure that the vector returned is has fewer Y's than there are recorded workers.
  #also, in some cases the probabilities don't really fit the structure of the group (this happens when the probabilities have been suppressed
  #due to low numbers). An example of this would be an SA2/triple with 2 individuals, 1 student, but both need to be workers. 
  #If the probability of a student being a worker is derived from the TA or NA probability it could be low, which makes it unlikely
  #that we will meet the target of two workers. So this function will also up the probability the more tries are needed to deal with
  #these cases
   
  guess_student_workers <- function(prob_yes,count_yes,N, n_workers,count_individual,SA22018,ethnicity,sex,age_band){
    if(any(length(prob_yes)>1,
           length(N)>1,
           length(n_workers)>1,
           length(count_individual)>1,
           length(SA22018)>1,
           length(ethnicity)>1,
           length(sex)>1,
           length(age_band)>1
    )){
      print("check arguments are length 1")
      stop()
    }
    
    criteria_met <- F
    tried_count <- FALSE
    counter <- 0
    while(criteria_met == F){
      
      #if the probability of assigning a worker is 1, but there are fewer total workers than the size of the group, 
      # we can only assign n_workers Y, and set the rest as N
      #if this occurs, throw a warning - this may only happen if there's something not quite right with the workerEducation probabilities.
      #e.g., 2 students, probability of being a worker is 1, but only one worker.  
      if(prob_yes == 1 & n_workers < N){vec <- c(rep("Y",n_workers),rep("N",N-n_workers))
      print(paste0("potential issue with SA22018 == ", SA22018,
                   ", ethnicity == ",ethnicity,
                   ", sex == ", sex,
                   ", age_band == ", age_band)
      )
      criteria_met <- T
      break}
      
     
      # if we have no non-students who can be workers...
      if((count_individual-N)==0){# we need to make students work regardless of the probability  
        vec <- c(rep("Y",n_workers), rep("N",N-n_workers)) #assign all workers as students, left over students as N
        criteria_met <- T  #success
        break}
      
      #if the count from backfilling workerEducation triple counts looks okay, use that for the first go
      if(count_yes <= N & count_yes <= n_workers & tried_count == FALSE){
        vec <- c(rep("Y",count_yes),rep("N",N-count_yes))
       tried_count <- TRUE 
      }
      ###Otherwise we need to guess at a vector of student workers
      # we will use the probabilities to get a sample of student workers for that group.
      vec <- sample(c("Y","N"),
                    prob = c(prob_yes,1-prob_yes),
                    N,
                    replace = TRUE
      )
      
      
      # since this vector is a guess, we need to test if this is an acceptable fit
      
      #we're happy if this count of student workers and the left over non-students can meet the workforce needed 
      #as long as the number of student workers does not exceed the number of actual workers needed.
      if((count_individual-N)+length(vec[vec == "Y"]) >= n_workers & #if the count of non-students + student_worker >=n_workers
         length(vec[vec == "Y"])<=n_workers){ #and the number of student workers is fewer than or equal to n_workers
        criteria_met <- T  #success
        break}
      #If the vector is not acceptable, the loop will repeat and try again. 
      #keep count of the number of tries, if the count of tries gets too high...
      counter <- counter+1
      # we'll increase the probability of creating a vector with student workers present.
      if(counter%%100==0){
        prob_yes <- min(prob_yes+0.1, 1)}
      if(counter%%500==0){  
        print(paste0("Tried 500 times, probability incrementally upped by 0.1 (x5) to: ",
                     "prob_yes= ",prob_yes,
                     ", N= ", N,
                     ", n_workers= ", n_workers,
                     ", count_individual= ", count_individual,
                     ", SA22018= ", SA22018))
      }
      
      #if this doesn't work eventually, we'll force an error that will stop the function.
      if(counter==1000){
        print("failed") 
        stop()}
      
      
    }
    return(vec)
  }


#Now we will assign attributes to each individual

 individual_nodelist_new <- individual_nodelist %>%
  
    #add student attribute
    group_by(SA22018,age_band,sex,ethnicity) %>%
    #this sample function creates a vector of Y's matching the value in the student_count column, with the remainder of rows being N
    mutate(student = sample(c(rep("Y",first(count_students)),
                              rep("N",n()-first(count_students))
    ), n(),replace=FALSE)) %>%
    
    #add worker attribute
    #if child, they can't be a worker
    #mutate(worker = ifelse(age_band == "0-14","N",NA)) %>%
    #this will use the same sampling function, but will also use the probability of are student being a worker to inform allocation
    #this probability comes from workEducation_triples data.
    left_join(probability_of_student_being_worker_df, by = c("SA22018","ethnicity","sex","age_band","student")) %>%
    
    # #if count_workers is the same as number of in the group, everyone should be a worker
    # group_by(SA22018,age_band,sex,ethnicity) %>%
    # mutate(worker = ifelse(count_workers==n(),"Y",worker)) %>%
    # ungroup() %>%
    
    
    #now assign workers to the student population - this uses the guess_student_workers function outline above.
    group_by(SA22018,age_band,sex,ethnicity,student) %>%
    #for individuals, if they're a student, assign them work status == Y with some probability
    mutate(worker = ifelse(student=='Y',
                           guess_student_workers(prob_yes = first(probability_of_student_being_worker),
                                                 count_yes = first(count_workersEducation),
                                                 N = n(),
                                                 n_workers = first(count_workers),
                                                 count_individual = first(count_individual),
                                                 SA22018=first(SA22018),ethnicity=first(ethnicity),
                                                 sex=first(sex),age_band=first(age_band)
                           ),
                           NA)
    ) %>%
    ungroup() %>%
    
    #get current count of assigned workers for each group so we can take it away from our target left to assign
    group_by(SA22018,age_band,sex,ethnicity) %>%
    mutate(count_students_already_assigned_as_workers = sum(worker=="Y",na.rm = T)) %>%
    ungroup() %>% 
    
    
    #fill in the rest of workers needed for the non-student population
    group_by(SA22018,age_band,sex,ethnicity,student) %>%
    mutate(worker = ifelse(student=='N', #if non-student
                           #fill in with vector of size non-student size, containing the number of workers left to fill.
                           sample(x = c(rep("Y",first(count_workers)-first(count_students_already_assigned_as_workers)),
                                        rep("N",n()-(first(count_workers)-first(count_students_already_assigned_as_workers)))
                           ),
                           size = n(),
                           replace=FALSE),
                           worker) #else stay the same
    ) %>%
    ungroup() %>%
   select(-probability_of_student_being_worker,-count_students_already_assigned_as_workers)
 
 
return(individual_nodelist_new) 
}
  