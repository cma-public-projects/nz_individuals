
addNumber_of_jobs <- function(individual_nodelist,job_distribution_nz){
  
  
  # #job distribution
  # job_distribution_nz <- read_csv(here("inputs","job_distributions","df_AssociatedPBN_distribution_triple_nz_safe_Checked.csv")) %>%
  #   #make associated PBNs of 10 or more just be 10, and turn column to integer
  #   mutate(AssociatedPBNs = parse_number(AssociatedPBNs)) %>%
  #   mutate(Count_people = ifelse(Count_people == -999999,1,Count_people)) %>%
  #   group_by(sex,ethnicity,age_band) %>%
  #   mutate(probability = Count_people/sum(Count_people)) %>%
  #   select(-Count_people)
  
  individual_nodelist_new <- individual_nodelist %>%
  #add number of jobs for workers
  #this uses the probability distribution of number of jobs across NZ per triple.
  #we might be able to refine this by using TA level probabalities
  group_by(SA22018,age_band,sex,ethnicity,worker) %>%
    mutate(number_of_jobs = ifelse(worker == "N", 0, #if worker is N, make number of jobs 0
                                   sample(x = job_distribution_nz$AssociatedPBNs[job_distribution_nz$sex==first(sex)&
                                                                                 job_distribution_nz$ethnicity==first(ethnicity)&
                                                                                 job_distribution_nz$age_band==first(age_band)],
                                        size = n(),
                                        prob = job_distribution_nz$probability[job_distribution_nz$sex==first(sex)&
                                                                                 job_distribution_nz$ethnicity==first(ethnicity)&
                                                                                 job_distribution_nz$age_band==first(age_band)]
                                        ,replace=TRUE))
  ) %>%
  ungroup() 
  return(individual_nodelist_new)
}