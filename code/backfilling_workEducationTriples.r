backfill_workEducation <- function(triples_SA2_individuals, upweighting = 1,
                                   triples_workeducation_NZ,
                                   triples_workeducation_TA,
                                   triples_workeducation_SA2,
                                   concordance){
  
 
  library(here)
  library(tidyverse)
  
 # this data needs to be copied from Dropbox/data/IDI_raw_outputs/triples/workforceTriples into git-repo/inputs
# 
# #loading and tidying formatting - including creating factors for categorical variables in a consistent manner
# triples_workeducation_NZ <- read_csv(here("inputs","individual_workforceEducation_counts","workEducation_tripleCounts_nz_safe.csv"),
#                        col_types = cols(
#                         sex = col_factor(levels = c('F','M')),
#                         ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                         age_band = col_character(),
#                         work_ind = col_character(),
#                         education_ind = col_character(),
#                         triple_count_total = col_double())
#                        ) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
#   arrange(age_band,ethnicity,sex) %>%
#   select(age_band,ethnicity,sex,work_ind,education_ind,triple_count_total) 
#  
# 
# # fill in missing values with NA or zero
# triples_workeducation_NZ <- triples_workeducation_NZ %>%
#   complete(nesting(age_band,ethnicity,sex,work_ind,education_ind), fill = list(triple_count_total = 0)) %>%
#   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total)) %>%
#    #if count at NZ level is suppressed, replace with 1
#   mutate(triple_count_total = ifelse(is.na(triple_count_total), 1, triple_count_total))
# 
#   
# triples_workeducation_TA <- read_csv(here("inputs","individual_workforceEducation_counts","workEducation_tripleCounts_ta_safe.csv"),
#                         col_types = cols(
#                           ur_ta = col_double(),
#                           sex = col_factor(levels = c('F','M')),
#                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                           age_band = col_character(),
#                           work_ind = col_character(),
#                           education_ind = col_character(),
#                           triple_count_total = col_double()) 
#                       ) %>%
#   rename(TA2018 = ur_ta) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
#   arrange(TA2018,age_band,ethnicity,sex)%>%
#   select(TA2018,age_band,ethnicity,sex,work_ind,education_ind,triple_count_total)
#  
# # fill in missing values with NA or zero
# triples_workeducation_TA <- triples_workeducation_TA %>%
#   complete(TA2018,nesting(age_band,ethnicity,sex,work_ind,education_ind), fill = list(triple_count_total = 0)) %>%
#   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total))                        
# 
# triples_workeducation_SA2 <- read_csv(here("inputs","individual_workforceEducation_counts","workEducation_tripleCounts_sa2_safe.csv"),
#                        col_types = cols(
#                          ur_sa2 = col_double(),
#                          sex = col_factor(levels = c('F','M')),
#                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                           age_band = col_character(),
#                           work_ind = col_character(),
#                           education_ind = col_character(),
#                           triple_count_total = col_double()) 
#                       ) %>%
#   rename(SA22018 = ur_sa2) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+')))%>%
#   arrange(SA22018,age_band,ethnicity,sex)%>%
#   select(SA22018,age_band,ethnicity,sex,work_ind,education_ind,triple_count_total)
# 
# # fill in missing values with NA or zero
# triples_workeducation_SA2 <- triples_workeducation_SA2 %>%
#   complete(SA22018,nesting(age_band,ethnicity,sex,work_ind,education_ind), fill = list(triple_count_total = 0)) %>%
#   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total))


  # get TA 888 data -  this is complete apart from F/MP/60+ which is suppressed (i.e. between 1 and 5). I will replace this with a 3, for RR3 purposes.
  TA_888 <- triples_workeducation_TA %>%
    filter(TA2018=='888') %>%
    rename(triple_count_nonNZ=triple_count_total) %>%
    mutate(triple_count_nonNZ = if_else(is.na(triple_count_nonNZ), 3,triple_count_nonNZ))
  
  # # replacing the triple count in the NZ data with the count without 888
  triples_workeducation_NZ <- triples_workeducation_NZ %>%
    right_join(TA_888, by = c("age_band", "ethnicity", "sex", "work_ind","education_ind")) %>%
    mutate(triple_count_total = triple_count_total-triple_count_nonNZ) %>%
    select(-triple_count_nonNZ,-TA2018)
  
  # removing the 888 and 888888 from TA and SA2 dataframes
  
  triples_workeducation_TA <- triples_workeducation_TA %>%
    filter(TA2018!='888')
  
  triples_workeducation_SA2 <- triples_workeducation_SA2 %>%
    filter(SA22018!='888888')
  
  # as part of this 'tidying' we will also remove 999 and 999999 - since we do not reconstruct schools or workforce etc for them
  
  triples_workeducation_TA <- triples_workeducation_TA %>%
    filter(TA2018!='999')
  
  SA2_999list <- concordance %>%
    filter(TA2018 == '999') %>%
    pull(SA22018)
  
  triples_workeducation_SA2 <- triples_workeducation_SA2 %>%
    filter(SA22018!='999999') %>%
    filter(!(SA22018 %in% SA2_999list))
  
 
  
  
  triples_workeducation_NZ_fromTA <- triples_workeducation_TA %>%
    group_by(age_band,ethnicity,sex,work_ind,education_ind) %>%
    mutate(suppressed_TA_number = sum(is.na(triple_count_total))) %>%
    mutate(suppressed_TA = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(age_band,ethnicity,sex,work_ind,education_ind,suppressed_TA,suppressed_TA_number) %>%
    summarise(triple_count_total_fromTA = sum(triple_count_total, na.rm = TRUE)) %>%
    ungroup()
  
  
  # find missing values, and reconstruct known NA from zeros
  
  comparison_NZ_level <- triples_workeducation_NZ %>%
    right_join(triples_workeducation_NZ_fromTA,  by = c("age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
    mutate(missing_NZlevel = triple_count_total-triple_count_total_fromTA) %>%
    mutate(triple_count_total_fromTA = case_when(
      triple_count_total_fromTA==0 & suppressed_TA=='No' ~ 0,
      triple_count_total_fromTA==0 & suppressed_TA=='Yes' ~ 999999,
      TRUE ~    triple_count_total_fromTA
    )) %>%
    mutate(triple_count_total_fromTA = ifelse(triple_count_total_fromTA==999999,NA,triple_count_total_fromTA))
  
  # create a column that is the number of suppressed cells
  # create a column that tells you whether (for that triple) any of the SA2 level counts were suppressed (==NA)
  # then sum up SA2 counts to TA level (ignoring NAs)
  
  triples_workeducation_TA_fromSA2 <- triples_workeducation_SA2 %>%
    left_join(concordance) %>%
    group_by(TA2018,age_band,ethnicity,sex,work_ind,education_ind) %>%
    mutate(suppressed_SA2_number = sum(is.na(triple_count_total))) %>%
    mutate(suppressed_SA2 = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(TA2018,age_band,ethnicity,sex,work_ind,education_ind,suppressed_SA2,suppressed_SA2_number) %>%
    summarise(triple_count_total_fromSA2 = sum(triple_count_total, na.rm = TRUE)) %>%
    ungroup()
  
  
  # find missing values, and reconstruct known NA from zeros
  comparison_TA_level <- triples_workeducation_TA %>%
    right_join(triples_workeducation_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
    mutate(missing_TAlevel = triple_count_total-triple_count_total_fromSA2) %>%
    mutate(triple_count_total_fromSA2 = case_when(
      triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
      triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
      TRUE ~    triple_count_total_fromSA2
    )) %>%
    mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))
  
  
  triples_workeducation_NZ_fromSA2 <- triples_workeducation_SA2 %>%
    left_join(concordance) %>%
    group_by(age_band,ethnicity,sex,work_ind,education_ind) %>%
    mutate(suppressed_SA2_number = sum(is.na(triple_count_total))) %>%
    mutate(suppressed_SA2 = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(age_band,ethnicity,sex,work_ind,education_ind,suppressed_SA2,suppressed_SA2_number) %>%
    summarise(triple_count_total_fromSA2 = sum(triple_count_total, na.rm = TRUE)) %>%
    ungroup()
  
  
  comparison_NZ_level_fromSA2 <- triples_workeducation_NZ %>%
    right_join(triples_workeducation_NZ_fromSA2,  by = c("age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
    mutate(missing_NZlevel = triple_count_total-triple_count_total_fromSA2) %>%
    mutate(triple_count_total_fromSA2 = case_when(
      triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
      triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
      TRUE ~    triple_count_total_fromSA2
    )) %>%
    mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2)) %>%
    mutate(mean_per_suppressed = missing_NZlevel/suppressed_SA2_number)
  


#### Guessing the suppressed counts

#At this point we know that at TA level there are some missing when there was a lot of suppression. We use the number missing and divide by the number of suppressed cells to work out the mean number of extra people need to be added for that SA2 and triple combination. 

#Then we use a lognormal distribution with that mean and sample a number between 1 and 5. Because of rounding to an integer, and round above 5 or below 1, the mean won't quite work as expected, but as long as we choose the shape parameter alright then it will be very small areas under the PDF (distribution) that are outside 0.5 and 5.5. 
  
#  If there are already too many people in that triple, then they are set to the minimum possible (1). If the mean missing is greater than 5, then they are set to the maximum possible (5).
  
#  We need to first do this at TA level for the suppressed counts there. Then we move on to SA2 level.
  
  missing_per_TA <- comparison_NZ_level %>%
    mutate(mean_per_suppressed = if_else(suppressed_TA_number>0,missing_NZlevel/suppressed_TA_number,0)) %>%
    select(age_band, ethnicity, sex, work_ind,education_ind, mean_per_suppressed)
  
  
  # this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing anything.
  triples_workeducation_TA_guesses <- triples_workeducation_TA %>% 
    filter(is.na(triple_count_total)) %>%  #this gets the suppressed rows only
    # head(100) %>%
    left_join(missing_per_TA, by = c("age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
    mutate(sd = 1.2, 
           location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
           shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
    rowwise() %>%
    mutate(triple_count_total_guess = case_when(
      mean_per_suppressed<=1    ~ 1,
      mean_per_suppressed>=5    ~ 5,
      TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
    )
    ) %>%
    ungroup()
  
  triples_workeducation_TA_infill <- triples_workeducation_TA %>%
    left_join(triples_workeducation_TA_guesses %>% select(TA2018,age_band, ethnicity, sex, work_ind,education_ind, triple_count_total_guess),  by = c("TA2018","age_band", "ethnicity", "sex", "work_ind","education_ind")) %>%
    mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
    select(-triple_count_total) %>%
    rename(triple_count_total=triple_count_total_guess)
  

comparison_TA_level_infill <- triples_workeducation_TA_infill %>%
  right_join(triples_workeducation_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex", "work_ind","education_ind")) %>%
  mutate(missing_TAlevel = triple_count_total-triple_count_total_fromSA2) %>%
    mutate(triple_count_total_fromSA2 = case_when(
    triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
    triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
    TRUE ~    triple_count_total_fromSA2
      )) %>%
    mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))

## take number missing per suppressed cell in that TA and use a normal distribution around the mean of approximately the required number of people per suppressed cell

missing_per_SA2 <- comparison_TA_level_infill %>%
  mutate(mean_per_suppressed = if_else(suppressed_SA2_number>0,missing_TAlevel/suppressed_SA2_number,0)) %>%
  select(TA2018, age_band, ethnicity, sex,work_ind,education_ind, suppressed_SA2_number, mean_per_suppressed)

# this is very slow and could be sped up with a map call or anything relaly, rowwise is a terrible way to be doing this.
triples_workeducation_SA2_guesses <- triples_workeducation_SA2 %>% 
  left_join(triples_SA2_individuals, by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
  filter(is.na(triple_count_total)) %>%
  # left_join(minimumcount_SA2) %>%
  left_join(concordance, by = "SA22018") %>%
  # head(100) %>%
  left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex","work_ind","education_ind")) %>% 
  rowwise() %>%
  mutate(sd = 1.2,
         mean = mean_per_suppressed*upweighting,
         location =if_else(mean>1, log(mean^2 / sqrt(sd^2 + mean^2)),-0.445999), 
         shape = ifelse(mean>1,sqrt(log(1 + (sd^2 / mean^2))),0.9444565)) %>%
  rowwise() %>%
  mutate(triple_count_total_guess = case_when(
                                        mean_per_suppressed<=1    ~ 1,
                                        mean_per_suppressed>=5    ~ min(5,count),
                                        TRUE                      ~ max(1,min(count,round(rlnorm(n=1, location, shape))))
                                      )
         ) %>%
  ungroup()


triples_workeducation_SA2_infill <- triples_workeducation_SA2 %>%
  left_join(triples_workeducation_SA2_guesses %>% select(SA22018,age_band, ethnicity, sex, work_ind,education_ind, triple_count_total_guess), by = c("SA22018", "age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
  mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
  select(-triple_count_total) %>%
  rename(triple_count_total=triple_count_total_guess)



  triples_workeducation_SA2_infill_adjusted<- triples_workeducation_SA2_infill %>%
    left_join(triples_SA2_individuals,by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
    mutate(triple_count_total= pmin(triple_count_total,count))
  
  
  while(abs(sum(triples_workeducation_SA2_infill_adjusted$triple_count_total) - sum(triples_workeducation_NZ$triple_count_total)) > 100){
    if(sum(triples_workeducation_SA2_infill_adjusted$triple_count_total) - sum(triples_workeducation_NZ$triple_count_total) > 0){
      upweighting <- upweighting-0.005
    }else{
      upweighting <- upweighting+0.005
    }
    print(paste("Difference is:",sum(triples_workeducation_SA2_infill_adjusted$triple_count_total) - sum(triples_workeducation_NZ$triple_count_total),"Upweighting is set to:",upweighting))
    
    # this is very slow and could be sped up with a map call or anything relaly, rowwise is a terrible way to be doing this.
    triples_workeducation_SA2_guesses <- triples_workeducation_SA2 %>% 
      left_join(triples_SA2_individuals, by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
      filter(is.na(triple_count_total)) %>%
      # left_join(minimumcount_SA2) %>%
      left_join(concordance, by = "SA22018") %>%
      # head(100) %>%
      left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex","work_ind","education_ind")) %>% 
      rowwise() %>%
      mutate(sd = 1.2,
             mean = mean_per_suppressed*upweighting,
             location =if_else(mean>1, log(mean^2 / sqrt(sd^2 + mean^2)),-0.445999), 
             shape = ifelse(mean>1,sqrt(log(1 + (sd^2 / mean^2))),0.9444565)) %>%
      rowwise() %>%
      mutate(triple_count_total_guess = case_when(
        mean_per_suppressed<=1    ~ 1,
        mean_per_suppressed>=5    ~ min(5,count),
        TRUE                      ~ max(1,min(count,round(rlnorm(n=1, location, shape))))
      )
      ) %>%
      ungroup()
    
    
    triples_workeducation_SA2_infill <- triples_workeducation_SA2 %>%
      left_join(triples_workeducation_SA2_guesses %>% select(SA22018,age_band, ethnicity, sex, work_ind,education_ind, triple_count_total_guess), by = c("SA22018", "age_band", "ethnicity", "sex","work_ind","education_ind")) %>%
      mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
      select(-triple_count_total) %>%
      rename(triple_count_total=triple_count_total_guess)
    
    
    
    triples_workeducation_SA2_infill_adjusted<- triples_workeducation_SA2_infill %>%
      left_join(triples_SA2_individuals,by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      mutate(triple_count_total= pmin(triple_count_total,count))
    
  }
  
  
output <- triples_workeducation_SA2_infill_adjusted %>%
  rename(count_workersEducation = triple_count_total) %>%
  select(SA22018,age_band,sex,ethnicity,work_ind,education_ind,count_workersEducation) %>%
  #get all combos for each triple by filling missing as 0.
  complete(SA22018,sex,ethnicity,age_band,work_ind,education_ind,fill = list(count_workersEducation=0))  %>%
  #create new column for probability distribution for each SA2/triple group
  group_by(SA22018,age_band,sex,ethnicity) %>%
  mutate(probability = count_workersEducation/sum(count_workersEducation)) %>% 
  ungroup() %>%
  #rename worker and student cols
  rename(worker = "work_ind",
         student = "education_ind"
         )

output %>%
  write_csv(here("outputs",paste0("tripleCounts_workEducation_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")))

output %>%
  write_csv(here("outputs","tripleCounts_workEducation_sa2_backfilled.csv"))

# Manually Copy the folder `here("outputs",new_directory) into dropbox
# dir.create(file.path(dropbox_directory,"data/IDI_processed_outputs/triples/workEducationTriples"))
file.copy(from = here("outputs","tripleCounts_workEducation_sa2_backfilled.csv"),
          to   = file.path(dropbox_directory,
                        "data/IDI_processed_outputs/triples/workEducationTriples/tripleCounts_workEducation_sa2_backfilled.csv"),
          overwrite = TRUE
          )

file.copy(from = here("outputs",paste0("tripleCounts_workEducation_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
          to   = file.path(dropbox_directory,
                        "data/IDI_processed_outputs/triples/workEducationTriples",paste0("tripleCounts_workEducation_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
          overwrite = TRUE
          )


return(output)
}