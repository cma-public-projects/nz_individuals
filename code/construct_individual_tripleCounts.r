##This takes the code from backfilling_individualTriples_notebook.Rmd and creates a single R file that can be sourced to create the backfilled individual counts.
## it should only be run by sourcing from 'construct_individual_nodelist.Rmd'.

construct_individual_triple_counts <- function(triples_NZ,triples_TA,triples_SA2,concordance){
require(here)
require(tidyverse)

# getting list of pre-existing variables to know what to keep at the end
keep_list <- ls()

#Loading in the triples data, we make sure that ethnicity and sex are factors, with the order we would like 
#(so that it is consistent in all datasets - SA2, TA, and NZ), and create a factor for agebands after changing 60 to 60+.
#Suppressed cells are coded as '999999', since we can't take NA out of the IDI. We find these and change them to NA.


# this data needs to be copied from Dropbox/data/IDI_raw_outputs/triples/overallTriples into git-repo/inputs/individual_counts
#loading and tidying formatting - including creating factors for categorical variables in a consistent manner
# triples_NZ <- read_csv(here("inputs","individual_counts","tripleCounts_nz_safe_checked.csv"),
#                        col_types = cols(
#                         sex = col_factor(levels = c('F','M')),
#                         ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                         age_band = col_character(),
#                         triple_count_total = col_double())
#                        ) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
#   arrange(age_band,ethnicity,sex) %>%
#   select(age_band,ethnicity,sex,triple_count_total)
# 
# 
# triples_TA <- read_csv(here("inputs","individual_counts","tripleCounts_ta_safe_checked.csv"),
#                         col_types = cols(
#                           ur_ta = col_double(),
#                           sex = col_factor(levels = c('F','M')),
#                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                           age_band = col_character(),
#                           triple_count_total = col_double())
#                       ) %>%
#   rename(TA2018 = ur_ta) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
#   arrange(TA2018,age_band,ethnicity,sex)%>%
#   select(TA2018,age_band,ethnicity,sex,triple_count_total)
# 
# # fill in missing values with NA or zero
# triples_TA <- triples_TA %>%
#   complete(TA2018,nesting(age_band,ethnicity,sex), fill = list(triple_count_total = 0)) %>%
#   mutate(triple_count_total = ifelse(triple_count_total=='999999',NA,triple_count_total))
# 
# triples_SA2 <- read_csv(here("inputs","individual_counts","tripleCounts_sa2_safe_checked.csv"),
#                        col_types = cols(
#                          ur_sa2 = col_double(),
#                          sex = col_factor(levels = c('F','M')),
#                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
#                           age_band = col_character(),
#                           triple_count_total = col_double())
#                       ) %>%
#   rename(SA22018 = ur_sa2) %>%
#   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
#   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+')))%>%
#   arrange(SA22018,age_band,ethnicity,sex)%>%
#   select(SA22018,age_band,ethnicity,sex,triple_count_total)
# 
# # fill in missing values with NA or zero
# triples_SA2 <- triples_SA2 %>%
#   complete(SA22018,nesting(age_band,ethnicity,sex), fill = list(triple_count_total = 0)) %>%
#   mutate(triple_count_total = ifelse(triple_count_total=='999999',NA,triple_count_total))


# get TA 888 data -  this is complete apart from F/MP/60+ which is suppressed (i.e. between 1 and 5).
#I will replace this with a 3, for RR3 purposes.
TA_888 <- triples_TA %>%
  filter(TA2018=='888') %>%
  rename(triple_count_nonNZ=triple_count_total) %>%
  mutate(triple_count_nonNZ = if_else(is.na(triple_count_nonNZ), 3,triple_count_nonNZ))

#Remove these ~94,000 people from the dataset, as they only turn up in TA and SA2, this means removing them from NZ level manually. 
#We also remove the 'outside region' codes - TA code 999, and SA2 code 999999. 
#This is a much smaller number of people (all zeros except for 14 suppressed and 1 which is 6), so we do not try to correct for this at NZ level.

# replacing the triple count in the NZ data with the count without 888
triples_NZ <- triples_NZ %>%
  right_join(TA_888, by = c("age_band", "ethnicity", "sex")) %>%
  mutate(triple_count_total = triple_count_total-triple_count_nonNZ) %>%
  select(-triple_count_nonNZ,-TA2018)

# removing the 888 and 888888 from TA and SA2 dataframes

triples_TA <- triples_TA %>%
  filter(TA2018!='888')

triples_SA2 <- triples_SA2 %>%
  filter(SA22018!='888888')

# as part of this 'tidying' we will also remove 999 and 999999 - since we do not reconstruct schools or workforce etc for them

# removing the 999 and 999999 from TA and SA2 dataframes
triples_TA <- triples_TA %>%
  filter(TA2018!='999')

SA2_999list <- concordance %>%
  filter(TA2018 == '999') %>%
  pull(SA22018)

triples_SA2 <- triples_SA2 %>%
  filter(SA22018!='999999') %>%
  filter(!(SA22018 %in% SA2_999list))


#### Looking for missing data

# Aggregating the TA to NZ level to look at total populations missing,NB when 'missing_xxlevel' is negative,
# there are too many people at the next level down i.e. summing up SA2s to TA level gives you more than the TA level count.

# create a column that is the number of suppressed cells 
# create a column that tells you whether (for that triple) any of the TA level counts were suppressed (==NA)
# then sum up SA2 counts to TA level (ignoring NAs)

triples_NZ_fromTA <- triples_TA %>%
  group_by(age_band,ethnicity,sex) %>%
  mutate(suppressed_TA_number = sum(is.na(triple_count_total))) %>%
  mutate(suppressed_TA = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
  ungroup() %>%  
  group_by(age_band,ethnicity,sex,suppressed_TA,suppressed_TA_number) %>%
  summarise(triple_count_total_fromTA = sum(triple_count_total, na.rm = TRUE)) %>%
  ungroup()


# find missing values, and reconstruct known NA from zeros

comparison_NZ_level <- triples_NZ %>%
  right_join(triples_NZ_fromTA,  by = c("age_band", "ethnicity", "sex")) %>%
  mutate(missing_NZlevel = triple_count_total-triple_count_total_fromTA) %>%
  mutate(triple_count_total_fromTA = case_when(
    triple_count_total_fromTA==0 & suppressed_TA=='No' ~ 0,
    triple_count_total_fromTA==0 & suppressed_TA=='Yes' ~ 999999,
    TRUE ~    triple_count_total_fromTA
  )) %>%
  mutate(triple_count_total_fromTA = ifelse(triple_count_total_fromTA==999999,NA,triple_count_total_fromTA))

#We find that for almost all the triple combinations, 
#when we sum up the TA counts to NZ level we have a mix of higher and lower
#- except for triples that have suppression at TA level. (see Rmd for plots)


#Now looking at TA level from SA2 counts and NZ level from SA2 counts:
 
# create a column that is the number of suppressed cells
# create a column that tells you whether (for that triple) any of the SA2 level counts were suppressed (==NA)
# then sum up SA2 counts to TA level (ignoring NAs)

triples_TA_fromSA2 <- triples_SA2 %>%
  left_join(concordance) %>%
  group_by(TA2018,age_band,ethnicity,sex) %>%
  mutate(suppressed_SA2_number = sum(is.na(triple_count_total))) %>%
  mutate(suppressed_SA2 = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
  ungroup() %>%  
  group_by(TA2018,age_band,ethnicity,sex,suppressed_SA2,suppressed_SA2_number) %>%
  summarise(triple_count_total_fromSA2 = sum(triple_count_total, na.rm = TRUE)) %>%
  ungroup()


# find missing values, and reconstruct known NA from zeros
comparison_TA_level <- triples_TA %>%
  right_join(triples_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex")) %>%
  mutate(missing_TAlevel = triple_count_total-triple_count_total_fromSA2) %>%
  mutate(triple_count_total_fromSA2 = case_when(
    triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
    triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
    TRUE ~    triple_count_total_fromSA2
  )) %>%
  mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))


triples_NZ_fromSA2 <- triples_SA2 %>%
  left_join(concordance) %>%
  group_by(age_band,ethnicity,sex) %>%
  mutate(suppressed_SA2_number = sum(is.na(triple_count_total))) %>%
  mutate(suppressed_SA2 = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
  ungroup() %>%  
  group_by(age_band,ethnicity,sex,suppressed_SA2,suppressed_SA2_number) %>%
  summarise(triple_count_total_fromSA2 = sum(triple_count_total, na.rm = TRUE)) %>%
  ungroup()


comparison_NZ_level_fromSA2 <- triples_NZ %>%
  right_join(triples_NZ_fromSA2,  by = c("age_band", "ethnicity", "sex")) %>%
  mutate(missing_NZlevel = triple_count_total-triple_count_total_fromSA2) %>%
  mutate(triple_count_total_fromSA2 = case_when(
    triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
    triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
    TRUE ~    triple_count_total_fromSA2
  )) %>%
  mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))# %>%
  # mutate(mean_per_suppressed = missing_NZlevel/suppressed_SA2_number)


#We find that for TAs with no suppression at SA2 level there is a mix of over and under counting.
#As you would expect from RR3. This is a relief! (see Rmd file)
  
#Missing from SA2 level to NZ we have a lot but mostly MaoriPacific or Pacific ethnicities. (See Rmd for plots)


#### Guessing the suppressed counts

#At this point we know that at TA level there are some missing when there was a lot of suppression. 
# We use the number missing and divide by the number of suppressed cells to work out the mean number of extra people need to be added for that SA2 and triple combination. 

# Then we use a lognormal distribution with that mean and sample a number centred on that mean and round to an integer, and round above 5 to 5 or below 1 to 1. 
# Because of the rounding, the mean won't quite work as expected (see Rmd for plots),
# but mostly it will be very small areas under the PDF (distribution) that are outside 0.5 and 5.5. 


# If there are already too many people in that triple, then they are set to the minimum possible (1). If the mean missing is greater than 5, then they are set to the maximum possible (5).

# We need to first do this at TA level for the suppressed counts there. Then we move on to SA2 level.
missing_per_TA <- comparison_NZ_level %>%
  mutate(mean_per_suppressed = if_else(suppressed_TA_number>0,missing_NZlevel/suppressed_TA_number,0)) %>%
  select(age_band, ethnicity, sex, mean_per_suppressed)


# this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing anything.
triples_TA_guesses <- triples_TA %>% 
  filter(is.na(triple_count_total)) %>%  #this gets the suppressed rows only
  # head(100) %>%
  left_join(missing_per_TA, by = c("age_band", "ethnicity", "sex")) %>%
  mutate(sd = 1.2, 
         location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
         shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
  rowwise() %>%
  mutate(triple_count_total_guess = case_when(
    mean_per_suppressed<=1    ~ 1,
    mean_per_suppressed>=5    ~ 5,
    TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
  )
  ) %>%
  ungroup()

triples_TA_infill <- triples_TA %>%
  left_join(triples_TA_guesses %>% select(TA2018,age_band, ethnicity, sex, triple_count_total_guess),  by = c("TA2018","age_band", "ethnicity", "sex")) %>%
  mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
  select(-triple_count_total) %>%
  rename(triple_count_total=triple_count_total_guess)



#We now have a 'complete' (no NA) count at TA level. Now we do the same thing but at SA2 level.
comparison_TA_level_infill <- triples_TA_infill %>%
  right_join(triples_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex")) %>%
  mutate(missing_TAlevel = triple_count_total-triple_count_total_fromSA2) %>%
  mutate(triple_count_total_fromSA2 = case_when(
    triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
    triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
    TRUE ~    triple_count_total_fromSA2
  )) %>%
  mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))

## take number missing per suppressed cell in that TA and use a normal distribution around the mean of 
# approximately the required number of people per suppressed cell

missing_per_SA2 <- comparison_TA_level_infill %>%
  mutate(mean_per_suppressed = if_else(suppressed_SA2_number>0,missing_TAlevel/suppressed_SA2_number,0)) %>%
  select(TA2018, age_band, ethnicity, sex,suppressed_SA2_number, mean_per_suppressed)

# this is very slow and could be sped up with a map call or anything relaly, rowwise is a terrible way to be doing this.
triples_SA2_infill <- triples_SA2 %>% 
  filter(is.na(triple_count_total)) %>%
  # left_join(minimumcount_SA2) %>%
  left_join(concordance, by = "SA22018") %>%
  # head(100) %>%
  left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex")) %>% 
  rowwise() %>%
  mutate(sd = 1.2, 
         location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
         shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
  rowwise() %>%
  mutate(triple_count_total_guess = case_when(
    mean_per_suppressed<=1    ~ 1,
    mean_per_suppressed>=5    ~ 5,
    TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
  )
  ) %>%
  ungroup()


triples_SA2_infill <- triples_SA2 %>%
  left_join(triples_SA2_infill %>% select(SA22018,age_band, ethnicity, sex, triple_count_total_guess), by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
  mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total))

# Having run the code we consistently find that for MaoriPacific ethnicity, as age increased, it goes from not enough to too many in the infill data. 
# These are the triple combinations with the most missing (suppressed) values. 
# Not sure why, but it does match the mean_per_suppressed decreasing, and we know that at the lower end (1) the log normal will produce a few too many, so maybe this is related to that.



#Finally we replace the guess with 'count' and save as csv.
triples_SA2_individuals <- triples_SA2_infill %>%
  rename(count = triple_count_total_guess) %>%
  select(SA22018,age_band,sex,ethnicity,count) 

# save as csv files 
triples_SA2_individuals %>%
  write_csv(here("outputs",paste0("tripleCounts_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")))

triples_SA2_individuals %>%
  write_csv(here("outputs","tripleCounts_sa2_backfilled.csv"))


#Save backfilled data
# dir.create(file.path(dropbox_directory,"/data/IDI_processed_outputs/triples/overallTriples"))
file.copy(from = here("outputs","tripleCounts_sa2_backfilled.csv"),
          to   = file.path(dropbox_directory,
                        "data/IDI_processed_outputs/triples/overallTriples/tripleCounts_sa2_backfilled.csv"),
          overwrite = TRUE
)

file.copy(from = here("outputs",paste0("tripleCounts_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
          to   = file.path(dropbox_directory,
                        "data/IDI_processed_outputs/triples/overallTriples",paste0("tripleCounts_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
          overwrite = TRUE
)
return(triples_SA2_individuals)
}
# # add output to keep to the keep_list
# keep_list <- c(keep_list,"triples_SA2_individuals")
# 
# # then tidy up!!
# rm(list = setdiff(ls(), keep_list))
# 
