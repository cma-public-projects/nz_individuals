
join_and_expand_individual_nodelist <- function(triples_SA2_individuals,
                                        triples_SA2_dwellingAssigned,
                                        triples_SA2_smallDwellings,
                                        triples_SA2_workforce,
                                        triples_SA2_education) {
  ##first join inputs
  triples_SA2_withinfo <- purrr::reduce(.x = list(triples_SA2_individuals,
                                                  triples_SA2_dwellingAssigned,
                                                  triples_SA2_smallDwellings,
                                                  triples_SA2_workforce,
                                                  triples_SA2_education),
                                        .f = left_join,
                                        by = c("SA22018","age_band","sex","ethnicity")                                      
  ) %>%
    #if the join produces NAs, replace with 0 count
    replace_na(list(count_dwellingAssigned = 0, count_smallDwellings = 0, count_workers = 0, count_students=0))
  
  #Now expand rows and assign attributes
  print("Warning: The expandRows function produces multiple warnings that specific rows are dropped. The function works as expected (producing the correct output), but this should be checked at some point.")
  suppressWarnings(
  individual_nodelist <- triples_SA2_withinfo %>%
    #first take the count and disaggregate, so each row represents an individual. (keep the count column)
    expandRows(count = "count",drop = F) %>%
    rename(count_individual = "count")
  )
  return(individual_nodelist)
}