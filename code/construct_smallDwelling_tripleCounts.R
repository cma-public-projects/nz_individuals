##This takes the code from backfilling_smallDwellingsTriples_notebook.Rmd and creates a single R file that can be sourced to create the backfilled individual counts.
## it should only be run by sourcing from 'construct_individual_nodelist.Rmd'.

construct_smallDwellings_triple_counts <- function(triples_SA2_dwellingAssigned, upweighting = 1,
                                                   triples_smalldwelling_NZ,
                                                   triples_smalldwelling_TA,
                                                   triples_smalldwelling_SA2,
                                                   concordance){
  
  require(here)
  require(tidyverse)
  
  # getting list of pre-existing variables to know what to keep at the end
  # keep_list <- ls()
  
  
  #loading and tidying formatting -----
  
  # this data needs to be copied from Dropbox/data/IDI_raw_outputs/triples/smallDwellingsTriples into git-repo/RCode/inputs
  
  #Loading in the triples data, we make sure that ethnicity and sex are factors, with the order we would like 
  #(so that it is consistent in all datasets - SA2, TA, and NZ), and create a factor for agebands after changing 60 to 60+.
  #Suppressed cells are coded as '-999999', since we can't take NA out of the IDI. We find these and change them to NA.
  
  
  #loading and tidying formatting - including creating factors for categorical variables in a consistent manner
  # triples_smalldwelling_NZ <- read_csv(here("inputs","individual_dwelling_counts", "dwellingTriples_smallDwellings_nz_safe_V2.csv"),
  #                        col_types = cols(
  #                          sex = col_factor(levels = c('F','M')),
  #                          ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
  #                          age_band = col_character(),
  #                          small_dwelling = col_character(),
  #                          triple_count_total = col_double()) 
  # ) %>%
  #   filter(small_dwelling=='Y') %>%
  #   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
  #   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
  #   arrange(age_band,ethnicity,sex) %>%
  #   select(age_band,ethnicity,sex,triple_count_total)
  # 
  # # fill in missing values with NA or zero
  # triples_smalldwelling_NZ <- triples_smalldwelling_NZ %>%
  #   complete(nesting(age_band,ethnicity,sex), fill = list(triple_count_total = 0)) %>%
  #   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total))
  # 
  # 
  # triples_smalldwelling_TA <- read_csv(here("inputs","individual_dwelling_counts", "dwellingTriples_smallDwellings_ta_safe_V2.csv"),
  #                        col_types = cols(
  #                          ur_ta = col_double(),
  #                          sex = col_factor(levels = c('F','M')),
  #                          ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
  #                          age_band = col_character(),
  #                          small_dwelling = col_character(),
  #                          triple_count_total = col_double()) 
  # ) %>%
  #   rename(TA2018 = ur_ta) %>%
  #   filter(small_dwelling=='Y') %>%
  #   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
  #   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+'))) %>%
  #   arrange(TA2018,age_band,ethnicity,sex)%>%
  #   select(TA2018,age_band,ethnicity,sex,triple_count_total)
  # 
  # # fill in missing values with NA or zero
  # triples_smalldwelling_TA <- triples_smalldwelling_TA %>%
  #   complete(TA2018,nesting(age_band,ethnicity,sex), fill = list(triple_count_total = 0)) %>%
  #   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total))                        
  # 
  # triples_smalldwelling_SA2 <- read_csv(here("inputs","individual_dwelling_counts", "dwellingTriples_smallDwellings_sa2_safe_V2.csv"),
  #                         col_types = cols(
  #                           ur_sa2 = col_double(),
  #                           sex = col_factor(levels = c('F','M')),
  #                           ethnicity = col_factor(levels=c('MaoriPacific','Maori','Pacific','Other')),
  #                           age_band = col_character(),
  #                           small_dwelling = col_character(),
  #                           triple_count_total = col_double())  
  # ) %>%
  #   rename(SA22018 = ur_sa2) %>%
  #   filter(small_dwelling=='Y') %>%
  #   mutate(age_band = if_else(age_band == '60','60+',age_band)) %>%
  #   mutate(age_band = factor(age_band, levels = c('0-14','15-29','30-59','60+')))%>%
  #   arrange(SA22018,age_band,ethnicity,sex)%>%
  #   select(SA22018,age_band,ethnicity,sex,triple_count_total)
  # 
  # # fill in missing values with NA or zero
  # triples_smalldwelling_SA2 <- triples_smalldwelling_SA2 %>%
  #   complete(SA22018,nesting(age_band,ethnicity,sex), fill = list(triple_count_total = 0)) %>%
  #   mutate(triple_count_total = ifelse(triple_count_total=='-999999',NA,triple_count_total))
  
  # We also remove the 'outside region' codes - TA code 999, and SA2 code 999999. This is a much smaller number of people (all zeros or suppressed).
  # since we do not reconstruct schools or workforce etc for them
  
  #exclude rows missing llocation info
  triples_smalldwelling_TA <- triples_smalldwelling_TA %>%
    filter(TA2018!='999')
  
  SA2_999list <- concordance %>%
    filter(TA2018 == '999') %>%
    pull(SA22018)
  
  triples_smalldwelling_SA2 <- triples_smalldwelling_SA2 %>%
    filter(SA22018!='999999') %>%
    filter(!(SA22018 %in% SA2_999list))
  
  ## right from the start, adjust unsuppressed counts to be <= individuals this is often off by 3 (only ever 3) due to RR3 rounding
  triples_smalldwelling_SA2_adjusted<- triples_smalldwelling_SA2 %>%
    left_join(triples_SA2_dwellingAssigned,by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
    mutate(triple_count_total= pmin(triple_count_total,count_dwellingAssigned)) %>%
    select(!count_dwellingAssigned)
  
  unsuppressed_rounding <- sum(triples_smalldwelling_SA2$triple_count_total, na.rm = T)-sum(triples_smalldwelling_SA2_adjusted$triple_count_total, na.rm = T)
  
  
  # #### Looking for missing data --------
  # 
  # Aggregating the TA to NZ level to look at total populations missing,NB when 'missing_xxlevel' is negative, 
  # there are too many people at the next level down i.e. summing up SA2s to TA level gives you more than the TA level count.
  
  # create a column that is the number of suppressed cells 
  # create a column that tells you whether (for that triple) any of the TA level counts were suppressed (==NA)
  # then sum up SA2 counts to TA level (ignoring NAs)
  
  
  triples_smalldwelling_NZ_fromTA <- triples_smalldwelling_TA %>%
    group_by(age_band,ethnicity,sex) %>%
    mutate(suppressed_TA_number = sum(is.na(triple_count_total))) %>%
    mutate(suppressed_TA = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(age_band,ethnicity,sex,suppressed_TA,suppressed_TA_number) %>%
    summarise(triple_count_total_fromTA = sum(triple_count_total, na.rm = TRUE)) %>%
    ungroup()
  
  triples_smalldwelling_TA_fromSA2 <- triples_smalldwelling_SA2 %>%
    left_join(concordance) %>%
    group_by(TA2018,age_band,ethnicity,sex) %>%
    mutate(suppressed_SA2_number = sum(is.na(triple_count_total))) %>%
    mutate(suppressed_SA2 = if_else(is.na(sum(triple_count_total, na.rm = FALSE)),'Yes','No')) %>%
    ungroup() %>%  
    group_by(TA2018,age_band,ethnicity,sex,suppressed_SA2,suppressed_SA2_number) %>%
    summarise(triple_count_total_fromSA2 = sum(triple_count_total, na.rm = TRUE)) %>%
    ungroup()
  
  
  # find missing values, and reconstruct known NA from zeros
  
  comparison_NZ_level <- triples_smalldwelling_NZ %>%
    right_join(triples_smalldwelling_NZ_fromTA,  by = c("age_band", "ethnicity", "sex")) %>%
    mutate(missing_NZlevel = triple_count_total-triple_count_total_fromTA) %>%
    mutate(triple_count_total_fromTA = case_when(
      triple_count_total_fromTA==0 & suppressed_TA=='No' ~ 0,
      triple_count_total_fromTA==0 & suppressed_TA=='Yes' ~ 999999,
      TRUE ~    triple_count_total_fromTA
    )) %>%
    mutate(triple_count_total_fromTA = ifelse(triple_count_total_fromTA==999999,NA,triple_count_total_fromTA))
  
  ### Guessing suppressed counts TA level ----
  # At this point we know that at TA level there are some missing when there was a lot of suppression. We use the number missing and divide by the number of suppressed cells to work out the mean number of extra people need to be added for that SA2 and triple combination. 
  
  # Then we use a lognormal distribution with that mean and sample a number between 1 and 5. Because of rounding to an integer, and round above 5 or below 1, the mean won't quite work as expected, but as long as we choose the shape parameter alright then it will be very small areas under the PDF (distribution) that are outside 0.5 and 5.5. 
  
  # If there are already too many people in that triple, then they are set to the minimum possible (1). If the mean missing is greater than 5, then they are set to the maximum possible (5).
  
  # We need to first do this at TA level for the suppressed counts there. Then we move on to SA2 level.
  
  
  missing_per_TA <- comparison_NZ_level %>%
    mutate(mean_per_suppressed = if_else(suppressed_TA_number>0,missing_NZlevel/suppressed_TA_number,0)) %>%
    select(age_band, ethnicity, sex, mean_per_suppressed)
  
  
  # this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing anything.
  triples_smalldwelling_TA_guesses <- triples_smalldwelling_TA %>% 
    filter(is.na(triple_count_total)) %>%  #this gets the suppressed rows only
    # head(100) %>%
    left_join(missing_per_TA, by = c("age_band", "ethnicity", "sex")) %>%
    mutate(sd = 1.2, 
           location =if_else(mean_per_suppressed>1, log(mean_per_suppressed^2 / sqrt(sd^2 + mean_per_suppressed^2)),-0.445999), 
           shape = ifelse(mean_per_suppressed>1,sqrt(log(1 + (sd^2 / mean_per_suppressed^2))),0.9444565)) %>%
    rowwise() %>%
    mutate(triple_count_total_guess = case_when(
      mean_per_suppressed<=1    ~ 1,
      mean_per_suppressed>=5    ~ 5,
      TRUE                      ~ max(1,min(5,round(rlnorm(n=1, location, shape))))
    )
    ) %>%
    ungroup()
  
  triples_smalldwelling_TA_infill <- triples_smalldwelling_TA %>%
    left_join(triples_smalldwelling_TA_guesses %>% select(TA2018,age_band, ethnicity, sex, triple_count_total_guess),  by = c("TA2018","age_band", "ethnicity", "sex")) %>%
    mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
    select(-triple_count_total) %>%
    rename(triple_count_total=triple_count_total_guess)
  
  # We now have a 'complete' (no NA) count at TA level. And the totals are very similar to the NZ level totals.
  
  ### Guessing suppressed counts TA level ----
  
  
  # Now we do the same thing but at SA2 level. 
  
  comparison_TA_level_infill <- triples_smalldwelling_TA_infill %>%
    right_join(triples_smalldwelling_TA_fromSA2,  by = c("TA2018", "age_band", "ethnicity", "sex")) %>%
    mutate(missing_TAlevel = triple_count_total-triple_count_total_fromSA2) %>%
    mutate(triple_count_total_fromSA2 = case_when(
      triple_count_total_fromSA2==0 & suppressed_SA2=='No' ~ 0,
      triple_count_total_fromSA2==0 & suppressed_SA2=='Yes' ~ 999999,
      TRUE ~    triple_count_total_fromSA2
    )) %>%
    mutate(triple_count_total_fromSA2 = ifelse(triple_count_total_fromSA2==999999,NA,triple_count_total_fromSA2))
  
  ## take number missing per suppressed cell in that TA and use a normal distribution around the mean of approximately the required number of people per suppressed cell
  
  missing_per_SA2 <- comparison_TA_level_infill %>%
    mutate(mean_per_suppressed = if_else(suppressed_SA2_number>0,missing_TAlevel/suppressed_SA2_number,0)) %>%
    select(TA2018, age_band, ethnicity, sex, suppressed_SA2_number, mean_per_suppressed)
  
  ## first attempt
  
  #upweighting <- 1.75 
  # this is very slow and could be sped up with a map call or anything relaly, rowwise is a terrible way to be doing this.
  triples_smalldwelling_SA2_guesses <- triples_smalldwelling_SA2 %>% 
    left_join(triples_SA2_dwellingAssigned, by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
    filter(is.na(triple_count_total)) %>%
    # left_join(minimumcount_SA2) %>%
    left_join(concordance, by = "SA22018") %>%
    # head(100) %>%
    left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex")) %>% 
    rowwise() %>%
    mutate(sd = 1.2,
           mean = mean_per_suppressed*upweighting,
           location =if_else(mean>1, log(mean^2 / sqrt(sd^2 + mean^2)),-0.445999), 
           shape = ifelse(mean>1,sqrt(log(1 + (sd^2 / mean^2))),0.9444565)) %>%
    rowwise() %>%
    mutate(triple_count_total_guess = case_when(
      mean_per_suppressed<=1    ~ 1,
      mean_per_suppressed>=5    ~ min(count_dwellingAssigned,5),
      TRUE                      ~ max(1,min(count_dwellingAssigned,round(rlnorm(n=1, location, shape))))
    )
    ) %>%
    ungroup()
  
  triples_smalldwelling_SA2_infill <- triples_smalldwelling_SA2 %>%
    left_join(triples_smalldwelling_SA2_guesses %>% select(SA22018,age_band, ethnicity, sex, triple_count_total_guess), by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
    mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
    select(-triple_count_total) %>%
    rename(triple_count_total=triple_count_total_guess)
  
  
  ## adjust using population triples
  triples_smalldwelling_SA2_infill_adjusted<- triples_smalldwelling_SA2_infill %>%
     left_join(triples_SA2_dwellingAssigned,by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
     mutate(triple_count_total= pmin(triple_count_total,count_dwellingAssigned))
  
  
  while(abs(sum(triples_smalldwelling_SA2_infill_adjusted$triple_count_total) - (sum(triples_smalldwelling_NZ$triple_count_total)-unsuppressed_rounding)) > 100){
    if(sum(triples_smalldwelling_SA2_infill_adjusted$triple_count_total) - (sum(triples_smalldwelling_NZ$triple_count_total)-unsuppressed_rounding) > 0){
      upweighting <- upweighting-0.005
    }else{
      upweighting <- upweighting+0.005
    }
    print(paste("Difference is:",sum(triples_smalldwelling_SA2_infill_adjusted$triple_count_total) - (sum(triples_smalldwelling_NZ$triple_count_total)-unsuppressed_rounding),"Upweighting is set to:",upweighting))
    
    # this is very slow and could be sped up with a map call or anything really, rowwise is a terrible way to be doing this.
    triples_smalldwelling_SA2_guesses <- triples_smalldwelling_SA2 %>% 
      left_join(triples_SA2_dwellingAssigned, by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
      filter(is.na(triple_count_total)) %>%
      # left_join(minimumcount_SA2) %>%
      left_join(concordance, by = "SA22018") %>%
      # head(100) %>%
      left_join(missing_per_SA2, by = c("TA2018","age_band", "ethnicity", "sex")) %>% 
      rowwise() %>%
      mutate(sd = 1.2,
             mean = mean_per_suppressed*upweighting,
             location =if_else(mean>1, log(mean^2 / sqrt(sd^2 + mean^2)),-0.445999), 
             shape = ifelse(mean>1,sqrt(log(1 + (sd^2 / mean^2))),0.9444565)) %>%
      rowwise() %>%
      mutate(triple_count_total_guess = case_when(
        mean_per_suppressed<=1    ~ 1,
        mean_per_suppressed>=5    ~ min(count_dwellingAssigned,5),
        TRUE                      ~ max(1,min(count_dwellingAssigned,round(rlnorm(n=1, location, shape))))
      )
      ) %>%
      ungroup()
    
    
    triples_smalldwelling_SA2_infill <- triples_smalldwelling_SA2 %>%
      left_join(triples_smalldwelling_SA2_guesses %>% select(SA22018,age_band, ethnicity, sex, triple_count_total_guess), by = c("SA22018", "age_band", "ethnicity", "sex")) %>%
      mutate(triple_count_total_guess = if_else(is.na(triple_count_total),triple_count_total_guess,triple_count_total)) %>%
      select(-triple_count_total) %>%
      rename(triple_count_total=triple_count_total_guess)
    
    
    ## adjust using population triples
    triples_smalldwelling_SA2_infill_adjusted<- triples_smalldwelling_SA2_infill %>%
      left_join(triples_SA2_dwellingAssigned,by = c("SA22018", "age_band", "sex", "ethnicity")) %>%
      mutate(triple_count_total= pmin(triple_count_total,count_dwellingAssigned))
    
  }
  
  
  
  
  
  #Finally we replace the guess with 'count' and save as csv.
  triples_smalldwelling_SA2_smallDwellings <- triples_smalldwelling_SA2_infill_adjusted %>%
    rename(count_smallDwellings = triple_count_total) %>%
    select(SA22018,age_band,sex,ethnicity,count_smallDwellings) 
  
  # save as csv files 
  triples_smalldwelling_SA2_smallDwellings %>%
    write_csv(here("outputs",paste0("tripleCounts_smallDwellings_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")))
  
  triples_smalldwelling_SA2_smallDwellings %>%
    write_csv(here("outputs","tripleCounts_smallDwellings_sa2_backfilled.csv"))
  
  
  #Save backfilled data
  # dir.create(file.path(dropbox_directory,"/data/IDI_processed_outputs/triples/dwellingTriples_smallDwellings"))
  file.copy(from = here("outputs","tripleCounts_smallDwellings_sa2_backfilled.csv"),
            to   = file.path(dropbox_directory,
                          "data/IDI_processed_outputs/triples/dwellingTriples_smallDwellings/tripleCounts_smallDwellings_sa2_backfilled.csv"),
            overwrite = TRUE
  )
  
  file.copy(from = here("outputs",paste0("tripleCounts_smallDwellings_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
            to   = file.path(dropbox_directory,
                          "data/IDI_processed_outputs/triples/dwellingTriples_smallDwellings",paste0("tripleCounts_smallDwellings_sa2_backfilled_",format(Sys.time(), "%Y%m%d"),".csv")),
            overwrite = TRUE
  )
  
  return(triples_smalldwelling_SA2_smallDwellings)
}
# # add output to keep to the keep_list
# keep_list <- c(keep_list,"triples_smalldwelling_SA2_smallDwellings")
# 
# # then tidy up!!
# rm(list = setdiff(ls(), keep_list))

