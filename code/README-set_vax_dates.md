# Documentation for set_vax_dates.ipynb

### Loading in the data
After loading in the necessary modules and defining some functions, we then load
in our data. This takes the form of:

- A vaccine data file (e.g. 'IndividualVaxDates_2022-05-30.csv')
- A population scaling file (e.g. PopulationScalings_Census2018_ERP2021_DHB_age_eth.csv)

Each row of the vaccine data file defines a dose of the vaccine for a single
individual, with the following columns:

- ID: individual ID
- dhb2015_name: the DHB of the individual
- sa22018: the SA2 of the individual
- ethnicity: the ethnicity of the individual
- ageband: the ageband of the individual
- ACTIVITYDATE: the date when the individual received the vaccine dose
- DOSENUMBER: the dose number for the individual (i.e. 2 implies that it is the
individuals 2nd dose)

In the vaccine data file the number of rows corresponds to the total number of 
vaccine doses given in New Zealand. Each row of the population scaling file define 
a DHB, ethnicity, ageband triple and the relative scaling between the 2018 Census 
and the 2021 Estimated Resident Population (ERP). The columns in this file are:

- DHB_name: DHB
- PrioritisedEthnicity: ethnicity
- age_band: ageband
- proportion_diff_ERP: this defines the scaling between the size of the DHB,
ethnicity and ageband group population in the 2018 Census and the 2021 ERP (e.g. 
1.04 means that the ERP population is 4% larger than the 2018 Census)

For consistency, in the vaccine data, we change all occurences of "Hawke's Bay" 
to "Hawkes Bay"

### Loading in the individual nodelist
Next we load in the individual nodelist (e.g. 'individual_nodelist_with_attributes_and_travel_2022-02-15.csv').
This will be used to define the nodes in the network and our end-goal here is to
add a new column to this file that defines the vaccine dates for individuals. First,
we add a new column with the DHB's of each individual (using the file 'SA2_DHB_unique.csv'
to map from SA2 to DHB). Again, we ensure that all occurences of "Hawke's Bay" 
are changed to "Hawkes Bay".

### Making the initial vaccine date dictionary
As an initial step, we take every entry in the vaccine data file and convert it
to a dictionary where each key is a unique individual ID which contains details
of the dates of each vaccine dose that individual has had.

### Making the grouped vaccine date dictionary
Before making a grouped vaccine data dictionary, it is worth noting some edge cases
in our vaccine data. A small proportion (~2%) do not have an SA2 defined,
and an even smaller proportion do not have a DHB defined. We drop the instances where
we have no location detail (no SA2 or DHB). For ease of assigning dates, we only 
consider the ageband of the individual at the time of their first dose. So with 
the initial vaccine date dictionary storing all dose information, we can filter 
the vaccine date dataframe to only include the first doses for individuals. This
signficantly reduces the number of rows in our vaccine date data frame to the 
approximate number of unique individuals in NZ who have had a least one dose. 
For the remaining edge-cases with no SA2 defined, we will use the DHB to assign 
vaccine dates in the nodelist. With our filtered vaccine date dataframe, we can 
then create a grouped vaccine date dictionary with each key being a string of the 
form "DHB, SA2, ageband, ethnicity" and the entry for each key being a list of sorted 
lists of the vaccine dates taken from our initial vaccine date dictionary.

### Scaling the grouped vaccine date dictionary
As our vaccine date data is based on the ERP population, and our nodelist is based
on the Census 2018 population, we must scale our grouped vaccine date dictionary
down to match the Census 2018 population. We do this using the population scaling 
file. For each key in the grouped vaccine date dictionary, we get the population 
scaling for the DHB, ethnicity, and ageband combination, and then randomly remove 
a proportion of the vaccine date lists in that key corresponding to the scaling.
In some cases, the 2021 ERP population is smaller for a group than the 2018 Census
population, however in most cases this is a relatively small decrease and so we 
ignore these.

### Applying the vaccine dates to the nodelist
We now have a grouped vaccine date dictionary with sets of vaccine doses that we 
wish to assign to the individuals in the individual nodelist. We begin by looping 
through each individual in the individual nodelist and for their combination of 
DHB, SA2, age, and ethnicity, we pop a vaccine date off the grouped vaccine date 
dictionary and assign it to that individual. If the the vaccine date dictionary 
is empty for that combination, we try popping from the DHB, age, and ethnicity 
combination with no SA2 defined (NaN). If there is nothing to pop, we do not assign 
vaccine dates for that individual (NaN). After doing this, ~20% of individuals in 
the nodelist are still not assigned any vaccine dates. However, we still have 
vaccine dates remaining in the grouped vaccine date dictionary left to assign.

### Fixing edge cases (taking out ethnicity)
For the remaining vaccine dates to assign, we begin by taking out ethnicity and
assigning just by DHB, SA2 and age. To avoid over-vaccinating the Maori population
relative to the data we have a variable `m_count` that counts the number of vaccine 
doses assigned Maori in this stage and maxes out 23000 doses.

### Fixing edge_cases (taking out SA2)
After taking out ethnicity we still have some vaccine dates left to assign. We
next take out SA2 and assign just by DHB and age. Again we include the `m_count`
variable to ensure we are not over-vaccinating the Maori population relative to
the data.

### Assigning leftovers
For the remaining doses we attempt to assign doses to the 15-29, 30-59 and 60+
agebands just by DHB. We assume that the proportion of vaccinated individuals in
these three agebands is relatively similar across the DHBs and the small number
of remaining vaccine doses involved should be relatively inconsequential. 

### Print unassigned doses
Finally we print the remaining doses that we have not been able to assign. This 
should be ~5000 doses, of which about half are for 'Overseas and undefined' 
individuals.

### Save nodelist
We then save the nodelist with the additional `vaccine_dates` column to a csv.


