def set_vax_dates(vax_data, pop_scaling, ih_attributes, DHB_SA2_conc):
        #Load in modules
    import pandas as pd
    import numpy as np
    from datetime import datetime
    from collections import defaultdict
    import random
    import math

    #Define function for calculating the difference between two dates
    def days_between(d0, d1):
        d1 = datetime.strptime(d1, "%Y-%m-%d")
        return (d1 - d0).days

        #Define function for deleting n random items from a list
    def delete_rand_items(list,n):
        to_delete = set(random.sample(range(len(list)),n))
        return [x for i,x in enumerate(list) if not i in to_delete]

        #Define function for reformatting a list of numbers as a string
    def reformat(dat):
        dat_str = ''
        for c, i in enumerate(dat):
            if c == 0:
                dat_str = dat_str + str(i)
            else:
                dat_str = dat_str + ';' + str(i)
        return dat_str

        #Define function for assigning dates from a dict
    def find_date(d_dict, d_key, v_list, v_dates, maori_c):
        dates = d_dict[d_key]
        if len(dates) > 0:
            r_dat = d_dict[d_key].pop()
            if type(r_dat) is list and len(r_dat) > 0:
                r_dat = reformat(r_dat)
                if d_key.split(", ")[3] != 'Maori' or maori_c>29000:
                    v_list.append(r_dat)
                else:
                    v_dates[f"{d_key.split(', ')[0]}, {d_key.split(', ')[2]}, Other"].append(r_dat)
                    v_list.append(np.nan)
                    maori_c+=1
            else:
                v_list.append(np.nan)
        else:
            v_list.append(np.nan)
        return d_dict, v_list, v_dates, maori_c

        ### Loading in the data
    #vax_data = pd.read_csv('IndividualVaxDates_2022-05-30.csv', usecols = ['ID', 'dhb2015_name', 'sa22018_code', 'ethnicity', 'ageband', 'ACTIVITYDATE', 'DOSENUMBER'])
    vax_data = vax_data.replace("Hawke's Bay", "Hawkes Bay") #replace "Hawke's Bay" with "Hawkes Bay" for consistency
    #pop_scaling = pd.read_csv('PopulationScalings_Census2018_ERP2021_DHB_age_eth.csv')

        ### Loading in the individual nodelist
    #ih_attributes = pd.read_csv('individual_nodelist_with_attributes_and_travel_2022-02-15.csv')
    ih_attributes = ih_attributes.sample(frac=1).reset_index(drop=True)
    #DHB_SA2_conc = pd.read_csv('SA2_DHB_unique.csv', usecols = ['SA22018_V1', 'DHB2015_Na'])
    DHB_SA2_conc = DHB_SA2_conc.replace("Hawke's Bay", "Hawkes Bay")
    DHB_SA2_conc = DHB_SA2_conc.set_index('SA22018_V1').T.to_dict('list')
    ih_attributes['DHB_2015'] = ih_attributes['SA22018'].map(DHB_SA2_conc).str[0]

    ### Making the initial vaccine date dictionary
    date_dict_init = defaultdict(list)
    d0 = datetime.strptime('2022-01-01', "%Y-%m-%d")
    for i,ac in zip(vax_data['ID'].to_list(),vax_data['ACTIVITYDATE'].to_list()):
        t = days_between(d0, ac)
        date_dict_init[i].append(t)

        ### Making the grouped vaccine data dictionary
    date_dict = defaultdict(list)
    vax_data = vax_data[vax_data['dhb2015_name'].notnull()]
    vax_data = vax_data.drop_duplicates(subset = "ID", keep = 'first')
    for i,d,s,e,ag in zip(vax_data['ID'].to_list(),vax_data['dhb2015_name'].to_list(),vax_data['sa22018_code'].to_list(),
            vax_data['ethnicity'].to_list(),vax_data['ageband'].to_list()):
        if math.isnan(float(s)):
            date_dict[f"{d}, {float(s)}, {ag}, {e}"].append(sorted(date_dict_init[i]))
        else:
            date_dict[f"{d}, {int(float(s))}, {ag}, {e}"].append(sorted(date_dict_init[i]))

            ### Scaling the grouped vaccine data dictionary
    for i in date_dict:
        filtered = pop_scaling[(pop_scaling['DHB_name'] == i.split(", ")[0]) & (pop_scaling['PrioritisedEthnicity'] == i.split(", ")[3])
            & (pop_scaling['age_band'] == i.split(", ")[2])]
        if len(filtered['proportion_diff_ERP'].to_list()) > 0:
            prop = filtered['proportion_diff_ERP'].to_list()[0]
            get_rid = round(len(date_dict[i]) - (1/prop)*len(date_dict[i]))
            if get_rid > 0:
                date_dict[i] = delete_rand_items(date_dict[i],get_rid)
        else:
            continue

            ### Applying the vaccine dates to the nodelist
    date_dict_copy = dict(date_dict)
    vax_date_list = []
    m_count = 0
    other_vax_dates = defaultdict(list)
    for d,s,a,e in zip(ih_attributes['DHB_2015'].to_list(), ih_attributes['SA22018'].to_list(), ih_attributes['age_band'].to_list(), ih_attributes['ethnicity'].to_list()):
        if f"{d}, {s}, {a}, {e}" in date_dict_copy:
            dates = date_dict_copy[f"{d}, {s}, {a}, {e}"]
            if len(dates) > 0:
                r_dat = date_dict_copy[f"{d}, {s}, {a}, {e}"].pop()
                if type(r_dat) is list and len(r_dat) > 0:
                    r_dat = reformat(r_dat)
                    if e != 'Maori' or m_count>29000:
                        vax_date_list.append(r_dat)
                    else:
                        other_vax_dates[f"{d}, {a}, Other"].append(r_dat)
                        vax_date_list.append(np.nan)
                        m_count+=1
                elif f"{d}, {np.nan}, {a}, {e}" in date_dict_copy:
                    date_dict_copy, vax_date_list, other_vax_dates, m_count = find_date(date_dict_copy, f"{d}, {np.nan}, {a}, {e}", vax_date_list, other_vax_dates, m_count)
                else:
                    vax_date_list.append(np.nan)
            elif f"{d}, {np.nan}, {a}, {e}" in date_dict_copy:
                date_dict_copy, vax_date_list, other_vax_dates, m_count = find_date(date_dict_copy, f"{d}, {np.nan}, {a}, {e}", vax_date_list, other_vax_dates, m_count)
            else:
                vax_date_list.append(np.nan)
        elif f"{d}, {np.nan}, {a}, {e}" in date_dict_copy:
            date_dict_copy, vax_date_list, other_vax_dates, m_count = find_date(date_dict_copy, f"{d}, {np.nan}, {a}, {e}", vax_date_list, other_vax_dates, m_count)
        else:
            vax_date_list.append(np.nan)
    ih_attributes['vaccine_dates'] = vax_date_list

    ### Fixing edge_cases (taking out ethnicity)
    date_dict_copy_no_eth = dict(date_dict_copy)
    for i in list(date_dict_copy_no_eth):
        if f"{i.split(', ')[0]}, {i.split(', ')[1]}, {i.split(', ')[2]}" in date_dict_copy_no_eth:
            date_dict_copy_no_eth[f"{i.split(', ')[0]}, {i.split(', ')[1]}, {i.split(', ')[2]}"] += date_dict_copy_no_eth.pop(i)
        else:
            date_dict_copy_no_eth[f"{i.split(', ')[0]}, {i.split(', ')[1]}, {i.split(', ')[2]}"] = date_dict_copy_no_eth.pop(i)
    ind = 0
    m_count = 0
    for d,s,a,e in zip(ih_attributes['DHB_2015'].to_list(), ih_attributes['SA22018'].to_list(), ih_attributes['age_band'].to_list(), ih_attributes['ethnicity'].to_list()):
        if f"{d}, {s}, {a}" in date_dict_copy_no_eth and str(vax_date_list[ind]) == 'nan' and (m_count>23000 or e != 'Maori'):
            dates = date_dict_copy_no_eth[f"{d}, {s}, {a}"]
            if len(dates) > 0:
                r_dat = date_dict_copy_no_eth[f"{d}, {s}, {a}"].pop()
                if type(r_dat) is list and len(r_dat) > 0:
                    r_dat = reformat(r_dat)
                    vax_date_list[ind] = r_dat
        elif f"{d}, {s}, {a}" in date_dict_copy_no_eth and str(vax_date_list[ind]) == 'nan' and e == 'Maori':
            dates = date_dict_copy_no_eth[f"{d}, {s}, {a}"]
            if len(dates) > 0:
                r_dat = date_dict_copy_no_eth[f"{d}, {s}, {a}"].pop()
                if type(r_dat) is list and len(r_dat) > 0:
                    r_dat = reformat(r_dat)
                    if len(r_dat.split(";")) == 2:
                        vax_date_list[ind] = np.nan
                        other_vax_dates[f"{d}, {a}, Other"].append(r_dat)
                        m_count+=1
                    else:
                        vax_date_list[ind] = r_dat

        ind+=1
    ih_attributes['vaccine_dates'] = vax_date_list

    ### Fixing edge_cases (taking out SA2)
    date_dict_copy_no_eth_no_SA2 = dict(date_dict_copy_no_eth)
    for i in list(date_dict_copy_no_eth_no_SA2):
        if f"{i.split(', ')[0]}, {i.split(', ')[2]}" in date_dict_copy_no_eth_no_SA2:
            date_dict_copy_no_eth_no_SA2[f"{i.split(', ')[0]}, {i.split(', ')[2]}"] += date_dict_copy_no_eth_no_SA2.pop(i)
        else:
            date_dict_copy_no_eth_no_SA2[f"{i.split(', ')[0]}, {i.split(', ')[2]}"] = date_dict_copy_no_eth_no_SA2.pop(i)
    ind = 0
    for d,a,e in zip(ih_attributes['DHB_2015'].to_list(), ih_attributes['age_band'].to_list(), ih_attributes['ethnicity'].to_list()):
        if f"{d}, {a}" in date_dict_copy_no_eth_no_SA2 and str(vax_date_list[ind]) == 'nan' and (m_count>23000 or e != 'Maori'):
            dates = date_dict_copy_no_eth_no_SA2[f"{d}, {a}"]
            if len(dates) > 0:
                r_dat = date_dict_copy_no_eth_no_SA2[f"{d}, {a}"].pop()
                if type(r_dat) is list and len(r_dat) > 0:
                    r_dat = reformat(r_dat)
                    vax_date_list[ind] = r_dat
        elif f"{d}, {a}" in date_dict_copy_no_eth_no_SA2 and str(vax_date_list[ind]) == 'nan' and e == 'Maori':
            dates = date_dict_copy_no_eth_no_SA2[f"{d}, {a}"]
            if len(dates) > 0:
                r_dat = date_dict_copy_no_eth_no_SA2[f"{d}, {a}"].pop()
                if type(r_dat) is list and len(r_dat) > 0:
                    r_dat = reformat(r_dat)
                    if len(r_dat.split(";")) == 2:
                        vax_date_list[ind] = np.nan
                        other_vax_dates[f"{d}, {a}, Other"].append(r_dat)
                        m_count+=1
                    else:
                        vax_date_list[ind] = r_dat
        ind+=1
    ih_attributes['vaccine_dates'] = vax_date_list

    ### Assigning leftovers
    ind = 0
    for d,a,e in zip(ih_attributes['DHB_2015'].to_list(), ih_attributes['age_band'].to_list(), ih_attributes['ethnicity'].to_list()):
        if f"{d}, {a}, {e}" in other_vax_dates and str(vax_date_list[ind]) == 'nan':
            dates = other_vax_dates[f"{d}, {a}, {e}"]
            if len(dates) > 0:
                r_dat = other_vax_dates[f"{d}, {a}, {e}"].pop()
                vax_date_list[ind] = r_dat
        ind+=1
    ih_attributes['vaccine_dates'] = vax_date_list
    ind = 0
    for d,a,e in zip(ih_attributes['DHB_2015'].to_list(), ih_attributes['age_band'].to_list(), ih_attributes['ethnicity'].to_list()):
        if (a == '30-59' or a == '60+') and f"{d}, {'15-29'}, {e}" in other_vax_dates and str(vax_date_list[ind]) == 'nan':
            dates = other_vax_dates[f"{d}, {'15-29'}, {e}"]
            if len(dates) > 0:
                r_dat = other_vax_dates[f"{d}, {'15-29'}, {e}"].pop()
                vax_date_list[ind] = r_dat
        elif (a == '15-29' or a == '60+') and f"{d}, {'30-59'}, {e}" in other_vax_dates and str(vax_date_list[ind]) == 'nan':
            dates = other_vax_dates[f"{d}, {'30-59'}, {e}"]
            if len(dates) > 0:
                r_dat = other_vax_dates[f"{d}, {'30-59'}, {e}"].pop()
                vax_date_list[ind] = r_dat
        elif (a == '15-29' or a == '30-59') and f"{d}, {'60+'}, {e}" in other_vax_dates and str(vax_date_list[ind]) == 'nan':
            dates = other_vax_dates[f"{d}, {'60+'}, {e}"]
            if len(dates) > 0:
                r_dat = other_vax_dates[f"{d}, {'60+'}, {e}"].pop()
                vax_date_list[ind] = r_dat
        ind+=1
    ih_attributes['vaccine_dates'] = vax_date_list

    ### Print unassigned doses
    for i in date_dict_copy_no_eth_no_SA2:
        if len(date_dict_copy_no_eth_no_SA2[i])>0:
            print("Unassigned vaccine date patterns:")
            print(f'{i}: {len(date_dict_copy_no_eth_no_SA2[i])}')

    for i in other_vax_dates:
        if len(other_vax_dates[i])>0:
            print("Unassigned vaccine date patterns:")
            print(f'{i}: {len(other_vax_dates[i])}')

            ### Save nodelist
    #ih_attributes.to_csv('individual_nodelist_with_attributes_and_travel_and_vax_dates_2022-05-30.csv')

    return ih_attributes
