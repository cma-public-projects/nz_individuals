require(tidyverse)
require(here)

#function for rounding values but preserving the sum
round_preserve_sum <- function(x, digits = 0) {
  up <- 10 ^ digits
  x <- x * up
  y <- floor(x)
  indices <- tail(order(x-y), round(sum(x)) - sum(y))
  y[indices] <- y[indices] + 1
  y / up
}

#This function will take the count of people who traveled (M) and break it into N integers (N being the count of trips)
#according to some distribution. log normal seems to fit overall - potentially we can improve here. 
#function found here: https://stackoverflow.com/questions/24845909/generate-n-random-integers-that-sum-to-m-in-r
distribution_allocation <- function(N, M, Lwr=1, Upr=7, sd = 1, pos.only = TRUE) {
  #get values from distribution (log norm??)
  #vec <- rnorm(N, M/N, sd) #normal
  #vec <- rlnorm(N, M/N, sd) # log normal
  ##truncated log normal, where min trip number is 1 and max number is 7 (which would be 1 per day  of the week)
  vec <- EnvStats::rlnormTrunc(N, M/N, sd, min = Lwr,max = Upr) 
  
  if (abs(sum(vec)) == Inf) vec <- rep(M/N, N)
  #round the values from the distribution
  vec <- round(vec / sum(vec) * M)
  #rounding will throw some values away from the total we're after - call this deviation
  deviation <- M - sum(vec)
  #reallocate the deviation to sample of values so values add up to correct total
  for (. in seq_len(abs(deviation))) {
    vec[i] <- vec[i <- sample(N, 1)] + sign(deviation)
  }
  #If there are more trips to fill than people to fill them, we will get a 0 count regardless
  #to stop code from breaking, we will accommodate this.
  if (N>M) {while (any(vec < 0)) {
    negs <- vec <= 0
    pos  <- vec > 0
    vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
    vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
  }
    #otherwise we will keep shuffling until all vectors are > 0
  }else{
    while (any(vec <= 0)) {
      negs <- vec <= 0
      pos  <- vec > 1
      vec[negs][i] <- vec[negs][i <- sample(sum(negs), 1)] + 1
      vec[pos][i]  <- vec[pos ][i <- sample(sum(pos ), 1)] - 1
    }
  }
  vec
}




#function to combine long range links to individual nodelist
addLongRangeTravel <- function(input_individual_nodelist,
                               Table1,
                               Table2,
                               Table3
                               #WEEK
                               ){

  print("Adding indicator for long range travel to individuals.")
  #Load market view data on number of individual long range travelers per SA2
  # Table3 <- read.csv(here("inputs","proprietary_data","marketview","MarketView_Output3_clean.csv")) %>%
  #   #select one week
  #   filter(SEQWEEK == WEEK)
  # -  `PERC` is the percent of people (BNZ cardholders) who traveled outside of their TA during that week. 
  #derive probability from percentage columns
  Table3_probTravel <- Table3 %>%
    #percentage as prob
    mutate(Prob_Traveled = Perc_Traveled/100) %>%
    select(CUST_SA2,Prob_Traveled)  %>%
    filter(!is.na(Prob_Traveled))
  
   #Add indicator for long range travel
  print("Warning: Adding long range travel indicator throws warning 'longer object length is not a multiple of shorter object length'. The function works as expected, but this should be checked at some point.")
  suppressWarnings(
    individual_nodelist_traveled <- input_individual_nodelist %>%
    #some SA2s are missing from the travel probability table, so we will skip these for now
    mutate(missing_travel_prob = ifelse(SA22018 %in% Table3_probTravel$CUST_SA2, F,T)) %>%
    group_by(SA22018, age_band) %>%
    #get number of individuals who traveled/did not travel (force 0 if missing travel prob).
    mutate(n_Ys = ifelse(missing_travel_prob,  0,
                          round(n()*Table3_probTravel$Prob_Traveled[Table3_probTravel$CUST_SA2==SA22018])
                          ),
           n_Ns = n()-n_Ys) %>%
    mutate(Traveled_LongRange = ifelse(missing_travel_prob, "N", #if the SA2 doesn't have a prob, make traveled "N" 
                                       #otherwise assign from vector of Ys and Ns consistent with probabilities from "n_Ys" variable created above
                                       sample(x = c(rep("Y",first(n_Ys)),#get Ys
                                                    rep("N",first(n_Ns)) # get Ns
                                       ),
                                       size = n(),#do it for each individual per age band and sa2.
                                       replace=FALSE)) #replace false means our new column will be forced to match the probability.
    ) %>%
    ungroup() %>%
    select(-missing_travel_prob)
    )
  
  print("Adding number of trips to each individual who traveled.")
  
  #add number of trips for the specified week
  #first load in data on number of trips
  # Table1 <- read.csv(here("inputs","proprietary_data","marketview","MarketView_Output1_clean.csv")) %>%
  #   #select one week
  #   filter(SEQWEEK == WEEK)
  # get the mean average number of trips for SA2
  Table1_tripcount <- Table1 %>%
    group_by(CUST_SA2) %>%
    summarise(Count = sum(count)) %>%
    left_join(Table3, by = "CUST_SA2") %>%
    #total number of trips divided by the total number of distinct travelers. 
    mutate(avg_number_trips = Count/ppl_dist) %>%
    select(CUST_SA2,Count,ppl_dist,avg_number_trips) 
  
  #Now add the number of trips drawing from distribution of average number of trips. 
  #This uses the distribution allocation function above, which takes N number of people, and allocates individuals a number of trips based on
  # average expected for that group size (M) across a distribution. 
  #This function does produce warnings for each row, which needs to be fixed. In the mean time we'll print one warning and suppress the automated ones.
  print("Warning: Distribution allocation function throws warning 'longer object length is not a multiple of shorter object length'. The function works as expected, but this should be checked at some point.")
  suppressWarnings(
  individual_nodelist_traveled_number <-  individual_nodelist_traveled %>%
    group_by(SA22018, Traveled_LongRange) %>%
    #apply distribution allocation function
    mutate(number_of_trips = ifelse(Traveled_LongRange == "N",NA,
                                    distribution_allocation(N = n(), #number of people
                                                            #number of trips (number of people * average number of trips)
                                                            M = n()*first(Table1_tripcount$avg_number_trips[Table1_tripcount$CUST_SA2==SA22018]))
    )
    ) %>%
    ungroup()
)
  
  print("Adding travel locations to individuals' trips.")
  
  # For each number of trips, get a vector of locations they traveled to.
  #first load in trips + destinations data
  # Table2 <- read.csv(here("inputs","proprietary_data","marketview","MarketView_Output2_clean.csv")) %>%
  #   #select one week
  #   filter(SEQWEEK == WEEK)
  #caluculate probabilities of trip destinations
  Table2_region_probs <- Table2 %>%
    #have region and duration in format "TA,TA" where n TA = duration
    rowwise() %>%
    mutate("TRIP_REGION_DUR" = paste(rep(MERCH_TLA,TRIP_DUR),collapse = ";")) %>%
    ungroup() %>%
    #calculate probability by getting total number of trips for the SA2
    group_by(CUST_SA2) %>%
    mutate(SA2_count = sum(ppl_count)) %>%
    ungroup() %>%
    #and the number of each specific trip
    group_by(CUST_SA2,TRIP_REGION_DUR) %>%
    summarise(prob_trip = ppl_count/SA2_count) %>%
    ungroup()
  
  ### old method of allocating destinations 
  # individual_nodelist_traveled_regions <- individual_nodelist_traveled_number %>%
  #   rowwise() %>%
  #   mutate(travel_TA_locations = ifelse(Traveled_LongRange=="N", NA, 
  #                                       paste(sample(x = Table2_region_probs$TRIP_REGION_DUR[Table2_region_probs$CUST_SA2==SA22018],
  #                                                    size = number_of_trips,
  #                                                    prob = Table2_region_probs$prob_trip[Table2_region_probs$CUST_SA2==SA22018],
  #                                                    replace = T
  #                                       ),collapse = ";")
  #   )) %>%
  #   ungroup()
  
  ### New method of allocating destinations
  #This section creates a full set of destinations for all of the trips in an SA2 first, and the samples without replacement from that list to add to the node list.
  
  #first make dataframe to store new version of individual node list
  individual_nodelist_traveled_regions <- data.frame()
  
  #if any individuals have number of trips be 0, just make them not be travelers (this issue should be identified in the distribution check)
  # we'll print a warning here so we can decide if this is a big issue or not (normally small number of edge cases)
  test_0_trips <- individual_nodelist_traveled_number %>% filter(!is.na(number_of_trips) & number_of_trips == 0)
  if(nrow(test_0_trips)>0){print(paste0("Warning: ", nrow(test_0_trips), " travelers were given 0 number of trips. These are reclassified as non-travelers."))}
  # reclassify these edge cases as non-travelers.
  individual_nodelist_traveled_number_clean <- individual_nodelist_traveled_number %>%
    mutate(Traveled_LongRange = ifelse(!is.na(number_of_trips) & number_of_trips == 0, "N", Traveled_LongRange)) %>%
    mutate(number_of_trips = ifelse(number_of_trips == 0, NA, number_of_trips))
  
  #get length of SA2 list (we will use this for timing/printing progress)
  SA2_length <- length(unique(individual_nodelist_traveled_number_clean$SA22018))
  #time it
  Time1 <- Sys.time()
  #Go through each SA2 and allocate trips
  for(SA2_i in 1:SA2_length){
    # define SA2 for the loop
    SA2 <- unique(individual_nodelist_traveled_number_clean$SA22018)[SA2_i]
    
    #Get N individuals travelers for that SA2
    N_trips <- individual_nodelist_traveled_number_clean %>%
      filter(SA22018 == SA2, Traveled_LongRange == "Y") %>%
      #get number of trips per SA2
      summarise(n = sum(number_of_trips)) %>%
      select(n) %>% pull()
    
    #if there are no trips, then the new list will just be the same with NA for travel locations
    if(N_trips == 0){
      new_individual_nodelist <- individual_nodelist_traveled_number_clean %>%
        filter(SA22018 == SA2) %>%
        mutate(travel_TA_locations = NA)
      
      #but if there are trips...
    }else{
      #get travel probs for that SA2
      Table2_region_probs_SA2 <- Table2_region_probs %>% filter(CUST_SA2 == SA2) 
      
      # Based on the number of individuals, we want to calculate the number of individuals that took each trip according to the probabilities.
      # calculate number of individuals for each probability. Store as a table of counts per SA2.
      estimated_SA2_travel <- Table2_region_probs_SA2 %>%
        mutate(Estimated_number = round_preserve_sum(prob_trip*N_trips)) %>%
        select(CUST_SA2,TRIP_REGION_DUR,Estimated_number)
      
      #now select each SA2 and the new estimated count of it's occurrences, and expand to create one vector we can sample from without replacement.
      SA2_estimates<- estimated_SA2_travel %>%
        splitstackshape::expandRows(count = "Estimated_number")
      
      #apply distribution of travel locations to our individuals as new variable.
      new_individual_nodelist <- individual_nodelist_traveled_number_clean %>%
        filter(SA22018 == SA2, Traveled_LongRange == "Y") %>%
        #add an individual identifier variable
        mutate(ID = row_number()) %>%
        #split rows so each row is an individual trip 
        # (e.g., if an individual had 4 recording trips, they will now be recorded on 4 separate rows)
        splitstackshape::expandRows(count = "number_of_trips") %>%
        #sample a destination for each individual trip according to estimates we calculate above
        #sample function will mean that destinations are allocated randomly across the trips
        # (i.e., trip destinations won't be influenced by the individual's other trip destinations.)
        mutate(trip_TA_locations = sample(x = SA2_estimates$TRIP_REGION_DUR,
                                          size = N_trips,
                                          replace = F
        )
        ) %>%
        #recombine so that trip destinations are at individual level rather than trip level.
        #do this by grouping by all the individual level attributes we want to keep
        group_by(ID, SA22018,age_band,sex,ethnicity,
                 count_individual,count_dwellingAssigned,count_smallDwellings,count_workers,count_students,
                 student,count_workersEducation,worker,dwellingAssigned,smallDwelling,number_of_jobs,
                 n_Ys,n_Ns,Traveled_LongRange) %>%
        #and this combining all trip locations into one single string (separated by semi-colons)
        summarise(number_of_trips = n(),
                  travel_TA_locations = paste(trip_TA_locations,collapse = ";"),  .groups = 'keep') %>%
        ungroup() %>%
        #select relevant cols
        select(-ID)
      
        if(max(new_individual_nodelist$n_Ns)>0){
        #add non travelers onto the new node list
          new_individual_nodelist <- new_individual_nodelist %>%
            bind_rows(individual_nodelist_traveled_number_clean %>%
                  filter(SA22018 == SA2, Traveled_LongRange == "N") %>%
                  mutate(travel_TA_locations = NA)
          )
        }
      
      }
    #print a warning if any SA2 returns a new node list that is a different size to the original node list
    if(nrow(new_individual_nodelist)!=nrow(individual_nodelist_traveled_number_clean %>% filter(SA22018 == SA2))){
      print(paste0("Error: New node list is a different size for SA2 ",SA2))
    }
    #append resulting node list to main data frame
    individual_nodelist_traveled_regions <- bind_rows(individual_nodelist_traveled_regions,new_individual_nodelist)
    #print(paste0(SA2," complete"))
    #print summary at every 10% complete
    if(SA2_i%%round(SA2_length/10)==0){
      print(paste0("Loop ",SA2_i,": Finished SA2 ", SA2))}
    if(SA2_i%%round(SA2_length/10)==0){
      print(paste0("Time elapsed: ", format(round(Sys.time()-Time1,2)),
                   ". ", round((SA2_i/SA2_length)*100), " Percent complete."))}
  }
  #print time taken
  print(paste0("Total time to complete: ", format(round(Sys.time()-Time1,2))))
  
  if(nrow(individual_nodelist_traveled_regions) == nrow(input_individual_nodelist)){
    return(individual_nodelist_traveled_regions) 
    }else{print("ERROR: new nodelist is not the same length as old nodelist.")}

}