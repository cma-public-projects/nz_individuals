
addDwelling_attributes <- function(individual_nodelist){
  
  individual_nodelist_new <- individual_nodelist %>%
#Next add dwelling info.
#Since people in small dwellings must have dwelling assigned == Y, we will create dwelling assigned first and then make sure 
#we only allocate small dwelling == Y when dwelling assigned == Y. 
group_by(SA22018,age_band,sex,ethnicity) %>%
  #add dwelling assigned attribute
  mutate(dwellingAssigned = sample(c(rep("Y",first(count_dwellingAssigned)),
                                     rep("N",n()-first(count_dwellingAssigned))
  ), n(),replace=FALSE)) %>%
  
  ungroup() %>%
  #add small dwelling attribute (need to regroup with dwelling assigned so we only have small_dwelling == Y when dwelling_assigned == Y)
  group_by(SA22018,age_band,sex,ethnicity,dwellingAssigned) %>%
  #add small dwelling attribute
  mutate(smallDwelling = ifelse(dwellingAssigned == "N", "N", #if dwelling assigned is N, make small dwelling N
                                sample(c(rep("Y",first(count_smallDwellings)), #otherwise assign Y with sample
                                         rep("N",n()-first(count_smallDwellings))
                                ), n(),replace=FALSE))
  ) %>%
  ungroup() 
  
  return(individual_nodelist_new)
}