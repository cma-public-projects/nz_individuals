Inputs are used in the construction of the synthetic individual population called by the file `code/construct_individual_nodelist.Rmd`

The inputs are summarised as follows:

- `concordances`. Files `MB18_SA2_majority_concordance_IDI.csv` and  `SA2_TA18_REGC_concordance.csv` provide tables with geospatial codes and their labels.

- `individual_counts`. Files `tripleCounts_nz_safe_checked.csv`, `tripleCounts_ta_safe_checked.csv`, and `tripleCounts_sa2_safe_checked.csv` are originally accessible through the TPM's complexSystems dropbox, contact Steven Turnbull (s.turnbull@auckland.ac.nz) for information. The files provide counts of individuals at the intersection of geospatial unit, Ethnicity (O,M,P,MP), Age band (0-14,15-29,30-59,60+), and sex (M,F).

- The folders `individual_dwelling_counts`, `individual_education_counts`, `individual_workforce_counts`, and `individual_workforceEducation_counts` provide the same information as the tables found in `individual_counts`, with the addition of specific indicator variables derived from census 2018. These include dwelling status (whether individuals had a dwelling asssigned in census, and for individuals who did have a dwelling whether they lived in a small dwelling of <20), education status, and work status.

- The folder `individual_sector_counts` contains the triple count with sector information attached. This is not used in the node list code, but is used to link individuals to workplaces when edgelists are made. The `code` file `backfilling_workSector_triples.Rmd` uses these data sets and imputes missing values.

- `job_distributions`. Contains files related to the distribution of number of jobs held by individuals. 

- `proprietary_data`. Used for storing proprietary data that is not available for public access. `code/construct_individual_nodelist.Rmd` will employ proprietary data from MarketView to provide long range travel links to individuals in the data. Access to this data can be found in the dropbox at: `covid-19-sharedFiles\data\movementData\marketview\Cleaned`.

  The files `MarketView_Output1_clean.csv`, `MarketView_Output1_clean.csv` and `MarketView_Output1_clean.csv` should be put in a folder called `marketview` within the proprietary data folder if running the long range travel code.

  If access to these data is not possible, comment out the following code from `construct_individual_nodelist.Rmd`:

  - source("addLongRangeTravel_attributes.r")
    individual_nodelist <- addLongRangeTravel(individual_nodelist, WEEK = 202011)
  - Also remove all references to the variable `travel_TA_locations`.

- Contact s.turnbull@auckland.ac.nz for more information. 