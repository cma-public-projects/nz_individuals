# NZ_individuals

The following work describes the processes employed to create a synthetic population of Aotearoa New Zealand (NZ), where each 'individual' has socio-demographic attributes and a geospatial location that is representative of the NZ population.

## Instructions:

### 1) Download RStudio
- see https://posit.co/download/rstudio-desktop/ 

### 2) Generate Individual Nodelist
- Ensure that the repo is up to date on your machine. The notebook will save all files using the short ID of the git commit that is checked out in file/folder names, if you make changes locally and then run the notebook, it will still use the latest git commit ID. To make sure that file names are consistent with content, make sure to push any changes before running the notebook.

- Open `nz_individuals.Rproj`. This will open in RStudio. The `.Rproj` file ensures that all specified paths are relative to the main repo directory.

- Within RStudio, open `construct_individual_nodelist.Rmd`. This notebook will call all other code files and should run from start to finish to produce the node list.

- Ensure that all R packages are installed. these include: `here`,`tidyverse`,`splitstackshape`,`git2r`, `EnvStats`.

- Set up your  python interpreter.

    - Go to `tools` and `project options` and navigate to `Python`. Here, select the python interpreter. 

    - Whichever python interpreter you use should have the following packages installed: `pandas`, `numpy`.

    - We recommend using conda to create a new conda environment with the name `vax_dates` and install the required packages with the following code: `conda create --name vax_dates python=3.10 pandas numpy`. Then in RStudio, with `nz_individuals.RProj` as your project, go to `Tools` and `Project options` and navigate to `Python`. Then click `Select` for choosing a Python Interpreter and once it has found available options, click on the `Conda Environments` tab and select the `vax_dates` environment. Note, python 3.10 is not required, earlier versions of python 3 should work fine.

- Specify your local path to the dropbox folder `covid-19-sharedFiles/` in the file `code/local_dropbox_path.txt`. The file should be only one line long. After setting this string, the input files should be found for copying across. 

- Check the file names (lines 68-124). Ensure that the files have been made available offline on dropbox. If they are stored as "online-only" it will cause an error later on.

- Set the week specified for the Marketview data on line 115. 

- After all of these steps, you should be able to `Run All` chunks (this can be done by navigating to the top right where it says `Run`, and clicking `Run All` on the drop down menu). 
The code will take roughly 30-40 minutes to finish, and it should produce a folder with the node list and sense checking files in an `outputs` folder in the repo, as well as a copy that goes to the `individual/nodelists/` folder in dropbox. 

- Double check the nodelist and the associated sense checking files to make sure the file looks reasonable before using.


## Information on specific folders are detailed as follows:

### code

The `code` folder contains the R scripts for processing data found in `inputs`. 

Each `.r` script is ran through the one `.Rmd` file called: `construct_individual_nodelist.Rmd`. 

`construct_individual_nodelist.Rmd` requires the following packages to be installed

- library(here)
- library(tidyverse)
- library(splitstackshape)
- library(EnvStats)

This file will first run scripts to clean input data, and then works to create a synthetic "node list" (or set of individuals), where each row in a table represents an individual. This final table will be around 4.7 million rows, with each row providing a individual representation of the counts found in Census 2018. After the first step, each individual will have:

-  `SA22018` a SA2 (Statistical Area 2) location of usual residence;

- `sex` a sex (M,F);

- `age_band` an age band (0-14,15-29,30-59,60+); 

- `ethnicity` an ethnicity (Other, Maori, Pacific, MaoriPacific).

The current implementation then uses a number of other steps (different source `R` files) to add the following attributes to individuals:

- `dwellingAssigned` (Y/N) - does the individual have a dwelling assigned in the census?

- `smallDwelling` (Y/N) - does the individual have a small dwelling assigned in census? ("Y" will indicate they are assigned to dwelling of size <20, "N" will indicate they are assigned to a larger dwelling).

- `student` (Y/N) - is the individual in education?

- `worker` (Y/N) - is the individual in work?

- `travel_TA_locations` - will detail a set of travel locations to locations outside of the individuals resident TA. `NA` values indicate no long range travel.  

Additional code within the `Rmd` file is written to:

- shuffle student worker attributes so that they capture a representation of the student workforce at a national level. 
- make all 0-14 year olds students.
- prevent all 0-14 year olds being workers.


A single python file is called to add vaccination information to individuals:  
- `vaccine_dates` - a list of dates, the length of which is the number of vaccines doses someone has had. Dates are in days relative to 1st January 2022. Unvaccinated individuals have their `vaccine_dates` set to 999999

### inputs

All inputs will be wiped by the `construct_individual_nodelist.Rmd` code and the specified inputs copied across from dropbox to the inputs folder. This prevents the use of input files that are only stored locally and are not reproducible for the wider team.

### outputs

This folder is ignored by git.

Any outputs folder from the nodelist construction code will be saved here when `construct_individual_nodelist.Rmd` is run. 
These will include files with the suffix `backfilled` which are similar to the input counts data but suppressed values imputed. These files will be around 2mb, with one file around 10mb.

The final node list (`individual_nodelist.csv`) will also be saved in the outputs, and is around 300mb in size. 
Each file will be saved with the file name consisting of "individual_nodelist" + git commit ID + the date. 
After running `construct_individual_nodelist.Rmd` , the outputs folder will be of size ~645mb, and the final `individual_nodelist` and associated readme file will be copied to the correct location in Dropbox.


## Authors and acknowledgment

Authors (order alphabetical): James Gilmour, Emily Harvey, Dion O'Neale, Steven Turnbull

We would like to acknowledge the help of Statistics NZ, who processed the data. We would also like to acknowledge the help of Adrian Ortiz-Cervantes. This work was funded and supported by Te P\={u}naha Matatini and MBIE.

## License


Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a [Creative Commons Attribution-NonCommercial-ShareAlike 4.0
International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg



## Disclaimer

The results in this paper are not official statistics. They have been created for research purposes from the Integrated Data Infrastructure (IDI), managed by Statistics New Zealand. The opinions, findings, recommendations, and conclusions expressed in this paper are those of the author(s), not Statistics NZ. Access to the anonymised data used in this study was provided by Statistics NZ under the security and confidentiality provisions of the Statistics Act 1975. Only people authorised by the Statistics Act 1975 are allowed to see data about a particular person, household, business, or organisation, and the results in this paper have been confidentialised to protect these groups from identification and to keep their data safe. Careful consideration has been given to the privacy, security, and confidentiality issues associated with using administrative and survey data in the IDI. Further detail can be found in the Privacy impact assessment for the Integrated Data Infrastructure available from www.stats.govt.nz. 
